package model;

import java.sql.Connection;
import java.util.ArrayList;



import dao.Database;
import dao.Project;

import dto.*;



public class ProjectManager {
	
	//insert society member
	public void WriteSocietyMember(SocietyMember sdata){
		//ArrayList<User> feeds = null;
		try {
			System.out.println("b4 projmanager");    
			Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				project.addSocietyMembersDetails(connection, sdata);
		System.out.println("after projmanager");
		} catch (Exception e) {
			
		
		}
	}
	
	//display society member
	public ArrayList<SocietyMember> GetSocietyMemberDetails(String smId)throws Exception {
		ArrayList<SocietyMember> allsm= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				allsm=project.GetSocietyMemberDetails(connection, smId);
		
		} catch (Exception e) {
			throw e;
		}
		return allsm;
	}
	
	//insert family member
		public void WriteFamilyMember(MembersFamily sdata){
			//ArrayList<User> feeds = null;
			try {
				System.out.println("b4 projmanager");    
				Database database= new Database();
				    Connection connection = database.Get_Connection();
					Project project= new Project();
					project.addFamilyMembersDetails(connection, sdata);
			System.out.println("after projmanager");
			} catch (Exception e) {
				
			
			}
		}
		
		//display family member
		public ArrayList<MembersFamily> GetFamilyMemberDetails(String fmId)throws Exception {
			ArrayList<MembersFamily> allfm= null;
			try {
				    Database database= new Database();
				    Connection connection = database.Get_Connection();
					Project project= new Project();
					allfm=project.GetFamilyMemberDetails(connection, fmId);
			
			} catch (Exception e) {
				throw e;
			}
			return allfm;
		}
		
		
		//insert complaint
				public void WriteComplaint(Complaint sdata){
					//ArrayList<User> feeds = null;
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addComplaintdeatils(connection, sdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
				}
				
				//display complaint
				public ArrayList<Complaint> GetComplaintDetails(String cmId)throws Exception {
					ArrayList<Complaint> allfm= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allfm=project.GetComplaintDetails(connection, cmId);
					
					} catch (Exception e) {
						throw e;
					}
					return allfm;
				}
				
				//insert noticeboard

				public void WriteNoticeboard(Noticeboard sdata) throws Exception{
					// TODO Auto-generated method stub
					
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addNoticeboarddeatils(connection, sdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
					
				}
				
				//display noticeboarddetails

				public ArrayList<Noticeboard> GetNoticeboarddetails(String varNId)throws Exception {
					// TODO Auto-generated method stub
					ArrayList<Noticeboard> allnm= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allnm=project.GetNoticeboardDetails(connection, varNId);
					
					} catch (Exception e) {
						throw e;
					}
					return allnm;
				}
				
				//insert meertingtraking

				public void Writemeetingtraking(meetingtraking sdata)throws Exception {
					// TODO Auto-generated method stub
					
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addmeetingdeatils(connection, sdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
					
				}
				
				//display meeting traking

				public ArrayList<meetingtraking> Getmeetingtrakingdetails(String mid) throws Exception{
					// TODO Auto-generated method stub
					ArrayList<meetingtraking> allmt= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allmt=project.GetmeetingDetails(connection, mid);
					
					} catch (Exception e) {
						throw e;
					}
					return allmt;
				}
				
				//insert Achievement

				public void WriteAchievement(Achievement sdata)throws Exception {
					// TODO Auto-generated method stub
					
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addAchievementdetails(connection, sdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
					
					
				}
				
				//display achievement

				public ArrayList<Achievement> Getachievementdetails(String aid)throws Exception {
					// TODO Auto-generated method stub
					ArrayList<Achievement> allam= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allam=project.GetachievementDetails(connection, aid);
					
					} catch (Exception e) {
						throw e;
					}
					return allam;
				}
				
				//insert forum
				public void Writeforum(Forum sdata) throws Exception{
					// TODO Auto-generated method stub
					
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addforumdetails(connection, sdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
					
				}
				//display forum
				public ArrayList<Forum> Getforumdetails(String fid) throws Exception {
					// TODO Auto-generated method stub
					ArrayList<Forum> allam= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allam=project.Getforumdetails(connection, fid);
					
					} catch (Exception e) {
						throw e;
					}
					return allam;
				}
				
				//insert forumcomment

				public void Writeforumcomment(forumcomment sdata) throws Exception {
					// TODO Auto-generated method stub
					
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addforumcommentdetails(connection, sdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
					
				}
				
				//display forumcommentdetails
				public ArrayList<forumcomment> Getforumcommentdetails(String fcid)throws Exception {
					// TODO Auto-generated method stub
					ArrayList<forumcomment> allam= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allam=project.Getforumcommentdetails(connection, fcid);
					
					} catch (Exception e) {
						throw e;
					}
					return allam;
				}
				
				//insert maintenancecharge
				
				public void WriteMaintenanceCharge(MaintenanceCharge sdata) throws Exception{
					// TODO Auto-generated method stub
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addMaintenanceChargedetails(connection, sdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
					
					
				}
				
				//display maintenancecharge

				public ArrayList<MaintenanceCharge> GetMaintenanceChargedetails(String mid)throws Exception {
					// TODO Auto-generated method stub
					
					ArrayList<MaintenanceCharge> allmc= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allmc=project.GetMaintenanceChargedetails(connection, mid);
					
					} catch (Exception e) {
						throw e;
					}
					return allmc;
				}

				//insert maintenanceamount
				
				public void Writemaintenanceamount(maintenanceamount sdata) throws Exception {
					// TODO Auto-generated method stub
					
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						 Connection connection = database.Get_Connection();
						 Project project= new Project();
						project.addmaintenanceamountdetails(connection, sdata);
						System.out.println("after projmanager");
						
					} catch (Exception e) {
						
					
					}
					
				}
				
				//display maintenanceamount

				public ArrayList<maintenanceamount> Getmaintenanceamountdetails(String maid) throws Exception{
					// TODO Auto-generated method stub
					ArrayList<maintenanceamount> allmamt= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allmamt=project.GetmaintenanceamountDetails(connection, maid);
					
					} catch (Exception e) {
						throw e;
					}
					return allmamt;
				}

					//insert MaintenanceInvoice
				
				public void Writemaintenanceinvoice(MaintenanceInvoice sdata) throws Exception{
					// TODO Auto-generated method stub
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addMaintenanceInvoicedetails(connection, sdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
					
					
				}
				//display MaintenanceInvoice

				public ArrayList<MaintenanceInvoice> Getmaintenanceinvoicedetails(String maid) throws Exception{
					// TODO Auto-generated method stub
					ArrayList<MaintenanceInvoice> allmamt= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allmamt=project.Getmaintenanceinvoicedetails(connection, maid);
					
					} catch (Exception e) {
						throw e;
					}
					return allmamt;
				}
				//insert penaltycharge
				
				public void Writepenaltycharge(penaltycharge sdata) throws Exception{
					// TODO Auto-generated method stub
					
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						Connection connection = database.Get_Connection();
						Project project= new Project();
						project.addpenaltychargedetails(connection, sdata);
						System.out.println("after projmanager");
						
					} catch (Exception e) {
						
					
					}
					
				}
				
				//display penaltycharge

				public ArrayList<penaltycharge> Getpenaltychargedetails(String pid) throws Exception {
					// TODO Auto-generated method stub
					ArrayList<penaltycharge> allpcmt= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allpcmt=project.GetpenaltychargeDetails(connection, pid);
					
					} catch (Exception e) {
						throw e;
					}
					return allpcmt;
				}
				
				public ArrayList<Registration> GetUserDetails()throws Exception {
					ArrayList<Registration> UserDetails= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							UserDetails=project.GetUserDetails(connection);
					
					} catch (Exception e) {
						throw e;
					}
					return UserDetails;
				}
				
				public ArrayList<Registration> GetRegisteredUserDetails()throws Exception {
					ArrayList<Registration> RegisteredUserDetails= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							RegisteredUserDetails=project.GetRegisteredUserDetails(connection);
					
					} catch (Exception e) {
						throw e;
					}
					return RegisteredUserDetails;
				}
				

				public ArrayList<Registration> GetRegisterDetails()throws Exception {
					ArrayList<Registration> RegisterDetails= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							RegisterDetails=project.GetRegisterDetails(connection);
					
					} catch (Exception e) {
						throw e;
					}
					return RegisterDetails;
				}
				
				public void WriteRegisterData(Registration cdata){
					//ArrayList<User> feeds = null;
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addRegisterDetails(connection, cdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
				}
				
				public ArrayList<Profile> GetProfileDetails(int sm_id, String firstname)throws Exception {
					ArrayList<Profile> ProfileDetails= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							ProfileDetails=project.GetProfileDetails(connection, sm_id, firstname);
					
					} catch (Exception e) {
						throw e;
					}
					return ProfileDetails;
				}
				
				public Registration GetLoginDetails(String username, String password)throws Exception {
					Registration user= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							user=project.GetLoginDetails(connection, username,password);
					
					} catch (Exception e) {
						throw e;
					}
					return user;
				}
								
/*				//insert society member
				public void WriteSocietyMember(SocietyMember sdata){
					//ArrayList<User> feeds = null;
					try {
						System.out.println("b4 projmanager");    
						Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							project.addSocietyMembersDetails(connection, sdata);
					System.out.println("after projmanager");
					} catch (Exception e) {
						
					
					}
				}
				
				//display society member
				public ArrayList<SocietyMember> GetSocietyMemberDetails(String smId)throws Exception {
					ArrayList<SocietyMember> allsm= null;
					try {
						    Database database= new Database();
						    Connection connection = database.Get_Connection();
							Project project= new Project();
							allsm=project.GetSocietyMemberDetails(connection, smId);
					
					} catch (Exception e) {
						throw e;
					}
					return allsm;
				}
				
				//insert family member
					public void WriteFamilyMember(MembersFamily sdata){
						//ArrayList<User> feeds = null;
						try {
							System.out.println("b4 projmanager");    
							Database database= new Database();
							    Connection connection = database.Get_Connection();
								Project project= new Project();
								project.addFamilyMembersDetails(connection, sdata);
						System.out.println("after projmanager");
						} catch (Exception e) {
							
						
						}
					}
					
					//display family member
					public ArrayList<MembersFamily> GetFamilyMemberDetails(String fmId)throws Exception {
						ArrayList<MembersFamily> allfm= null;
						try {
							    Database database= new Database();
							    Connection connection = database.Get_Connection();
								Project project= new Project();
								allfm=project.GetFamilyMemberDetails(connection, fmId);
						
						} catch (Exception e) {
							throw e;
						}
						return allfm;
					}
					//insert classified details
					*/
					public void WriteClassified(Classified sdata){
						//ArrayList<User> feeds = null;
						try {
							System.out.println("b4 projmanager");    
							Database database= new Database();
							    Connection connection = database.Get_Connection();
								Project project= new Project();
								project.addClassifiedDetails(connection, sdata);
						System.out.println("after projmanager");
						} catch (Exception e) {
							
						
						}
					}
					//display classified details
					public ArrayList<Classified> GetClassifiedDetails(String cId)throws Exception {
						ArrayList<Classified> allclass= null;
						try {
							    Database database= new Database();
							    Connection connection = database.Get_Connection();
								Project project= new Project();
								allclass=project.GetClassifiedDetails(connection, cId);
						
						} catch (Exception e) {
							throw e;
						}
						return allclass;
					}
					
					//insert event details
					public void WriteEventDetails(Event sdata){
						//ArrayList<User> feeds = null;
						try {
							System.out.println("b4 projmanager");    
							Database database= new Database();
							    Connection connection = database.Get_Connection();
								Project project= new Project();
								project.addEventDetails(connection, sdata);
						System.out.println("after projmanager");
						} catch (Exception e) {
							
						
						}
					}
					
					//display Event details
							public ArrayList<Event> GetEventDetails(String eId)throws Exception {
								ArrayList<Event> allevent= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allevent=project.GetEventDetails(connection, eId);
								
								} catch (Exception e) {
									throw e;
								}
								return allevent;
							}
					//insert event expense details
							public void WriteEventExpense(EventExpense sdata){
								//ArrayList<User> feeds = null;
								try {
									System.out.println("b4 projmanager");    
									Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										project.addEventExpenseDetails(connection, sdata);
								System.out.println("after projmanager");
								} catch (Exception e) {
									
								
								}
							}
						//display event expense details
							public ArrayList<EventExpense> GetEventExpenseDetails(String eeId)throws Exception {
								ArrayList<EventExpense> allee= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allee=project.GetEventExpenseDetails(connection, eeId);
								
								} catch (Exception e) {
									throw e;
								}
								return allee;
							}
					
						//insert event income details
							public void WriteEventIncome(EventIncome sdata){
								//ArrayList<User> feeds = null;
								try {
									System.out.println("b4 projmanager");    
									Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										project.addEventIncomeDetails(connection, sdata);
								System.out.println("after projmanager");
								} catch (Exception e) {
									
								
								}
							}
							
							//display event income details
							public ArrayList<EventIncome> GetEventIncomeDetails(String eiId)throws Exception {
								ArrayList<EventIncome> allei= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allei=project.GetEventIncomeDetails(connection, eiId);
								
								} catch (Exception e) {
									throw e;
								}
								return allei;
							}
							
							//insert booking details
							public void WriteBooking(Booking sdata){
								//ArrayList<User> feeds = null;
								try {
									System.out.println("b4 projmanager");    
									Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										project.addBookingDetails(connection, sdata);
								System.out.println("after projmanager");
								} catch (Exception e) {
									
								
								}
							}
							//display booking details
							public ArrayList<Booking> GetBookingDetails(String bId)throws Exception {
								ArrayList<Booking> allbooking= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allbooking=project.GetBookingDetails(connection, bId);
								
								} catch (Exception e) {
									throw e;
								}
								return allbooking;
							}
							//insert polling details
							public void WritePolling(Polling sdata){
								try {
									System.out.println("b4 projmanager");    
									Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										project.addPolling(connection, sdata);
								System.out.println("after projmanager");
								} catch (Exception e) {
									
								
								}
							}
							//display polling details
							public ArrayList<Polling> GetPollingDetails(String pId)throws Exception {
								ArrayList<Polling> allpolling= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allpolling=project.GetPollingDetails(connection, pId);
								
								} catch (Exception e) {
									throw e;
								}
								return allpolling;
							}
							//insert vendor details
							public void WriteVendor(Vendor sdata){
								try {
									System.out.println("before projmanager");    
									Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										project.addVendor(connection, sdata);
								System.out.println("after projmanager");
								} catch (Exception e) {
									
								
								}
							}
							//display vendor details
							public ArrayList<Vendor> GetVendorDetails(String vId)throws Exception {
								ArrayList<Vendor> allvendor= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allvendor=project.GetVendorDetails(connection, vId);
								
								} catch (Exception e) {
									throw e;
								}
								return allvendor;
							}
							
							//insert ServiceOrder details
							
							public void WriteServiceOrder(ServiceOrder sdata){
								try {
									System.out.println("before projmanager");    
									Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										project.addServiceOrder(connection, sdata);
								System.out.println("after projmanager");
								} catch (Exception e) {
									
								
								}
							}
							//display  ServiceOrder  details
							public ArrayList<ServiceOrder> GetServiceOrderDetails(String soId)throws Exception {
								ArrayList<ServiceOrder> allservice= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allservice=project.GetServiceOrderDetails(connection, soId);
								
								} catch (Exception e) {
									throw e;
								}
								return allservice;
							}
								//insert Bill Payment details
							
							public void WriteBillPayment(BillPayment sdata){
								try {
									System.out.println("before projmanager");    
									Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										project.addBillPayment(connection, sdata);
								System.out.println("after projmanager");
								} catch (Exception e) {
									
								
								}
							}
							//display  Bill Payment  details
							public ArrayList<BillPayment> GetBillPaymentDetails(String bpId)throws Exception {
								ArrayList<BillPayment> allbill= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allbill=project.GetBillPaymentDetails(connection, bpId);
								
								} catch (Exception e) {
									throw e;
								}
								return allbill;
							}
							//insert VehicleRegistration details

							public void WriteVehicleRegistration(VehicleRegistration sdata){
								try {
									System.out.println("before projmanager");    
									Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										project.addVehicleRegistration(connection, sdata);
								System.out.println("after projmanager");
								} catch (Exception e) {
									
								
								}
							}
							//display  Vehicle Registration  details
							public ArrayList<VehicleRegistration> GetVehicleDetails(String vrId)throws Exception {
								ArrayList<VehicleRegistration> allvehicle= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allvehicle=project.GetVehicleDetails(connection, vrId);
								
								} catch (Exception e) {
									throw e;
								}
								return allvehicle;
							}
							//display  SocietyRules details
							public ArrayList<SocietyRules> GetSocietyDetails(String srId)throws Exception {
								ArrayList<SocietyRules> allrules= null;
								try {
									    Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										allrules=project.GetSocietyDetails(connection, srId);
								
								} catch (Exception e) {
									throw e;
								}
								return allrules;
							}
							
							
							
							public void ApproveEvent(Event sdata){
								//ArrayList<User> feeds = null;
								try {
									System.out.println("b4 projmanager");    
									Database database= new Database();
									    Connection connection = database.Get_Connection();
										Project project= new Project();
										project.ApproveEvent(connection, sdata);
								System.out.println("after projmanager");
								} catch (Exception e) {
									
								
								}
							}
				
				
		
}
