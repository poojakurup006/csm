package dao;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

//pooja
import dto.*;


public class Project {
	
	//insert society  members
	public void addSocietyMembersDetails(Connection connection,SocietyMember sdata) throws SQLException{
		System.out.print("inside add society members details");
		String sqlCommand = "Insert into tbl_society_member (firstname,lastname,address,email,mobile_no,no_of_family_mem,Is_tenant,username,password) values(' " + sdata.getFirstname()+"',' " +sdata.getLastname()+"', ' " +sdata.getAddress()+"', ' " +sdata.getEmail()+"',"+sdata.getMobile_no() +","+sdata. getNo_of_fm_mem()+","+sdata.getIs_tenant()+",' " +sdata.getUsername()+"', ' " +sdata.getPassword()+"');";
		//String sqlCommand = "Insert into demo (firstname,lastname,address) values('" + sdata.getFirstname()+"',' " +sdata.getLastname()+"',' " +sdata.getAddress()+"')";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
		//display society members
	   public ArrayList<SocietyMember>  GetSocietyMemberDetails(Connection connection,String smId) throws Exception
	 	{
	 		ArrayList<SocietyMember>  allsm =new ArrayList<SocietyMember>();
	 		try
	 		{
	 			
	 			String query ="select firstname,lastname,address,email,mobile_no,no_of_family_mem,Is_tenant,image,username,password from tbl_society_member ";
	 			
	 			System.out.println(query);
	 			PreparedStatement ps = connection.prepareStatement(query);
	 			//ps.setString(1,uname);
	 			ResultSet rs = ps.executeQuery();
	 			while(rs.next())
	 			{
	 				SocietyMember sm = new SocietyMember();
	 				sm.setFirstname(rs.getString("firstname"));
	 				sm.setLastname(rs.getString("lastname"));
	 				sm.setAddress(rs.getString("address"));
	 				sm.setEmail(rs.getString("email"));
	 				sm.setMobile_no(rs.getInt("mobile_no"));
	 				sm.setNo_of_fm_mem(rs.getInt("no_of_family_mem"));
	 				sm.setIs_tenant(rs.getBoolean("Is_tenant"));
	 				//sm.setImage(rs.getBlob("image"));
	 				sm.setUsername(rs.getString("username"));
	 				sm.setPassword(rs.getString("password"));
	 				
	 				allsm.add(sm);
	 				
	 			}
	 			return allsm;
	 		}
	 		catch(Exception e)
	 		{
	 			throw e;
	 		}
	 }
	   
	   //insert family members
	   public void addFamilyMembersDetails(Connection connection,MembersFamily sdata) throws SQLException{
			System.out.print("inside add family members details");
			String sqlCommand = "Insert into tbl_members_family (firstname,lastname,age,profession,rel_with_owner,gender,pancard_no) values(' " + sdata.getFirstname()+"',' " +sdata.getLastname()+"',  " +sdata.getAge()+", ' "+sdata.getProfession() +" ',' " +sdata.getRel_with_owner()+"',' "+sdata.getGender() +" ', ' "+sdata.getPan_no()+" ' );";
			
			System.out.print(sqlCommand);
			Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
			try {
				boolean insert = ps.execute(sqlCommand);
				System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	   
		//display society members
	   public ArrayList<MembersFamily>  GetFamilyMemberDetails(Connection connection,String fmId) throws Exception
	 	{
	 		ArrayList<MembersFamily>  allfm =new ArrayList<MembersFamily>();
	 		try
	 		{
	 			
	 			String query ="select firstname,lastname,age,profession,rel_with_owner,gender,dob,pancard_no from tbl_members_family ";
	 			
	 			System.out.println(query);
	 			PreparedStatement ps = connection.prepareStatement(query);
	 		
	 			ResultSet rs = ps.executeQuery();
	 			while(rs.next())
	 			{
	 				MembersFamily fm = new MembersFamily();
	 				fm.setFirstname(rs.getString("firstname"));
	 				fm.setLastname(rs.getString("lastname"));
	 				fm.setAge(rs.getInt("age"));
	 				fm.setProfession(rs.getString("profession"));
	 				fm.setRel_with_owner(rs.getString("rel_with_owner"));
	 				fm.setGender(rs.getString("gender"));
	 				fm.setDob(rs.getDate("dob"));
	 				fm.setPan_no(rs.getString("pancard_no"));
	 				allfm.add(fm);
	 			}
	 			return allfm;
	 		}
	 		catch(Exception e)
	 		{
	 			throw e;
	 		}
	 }

	   //insert complaint
	public void addComplaintdeatils(Connection connection, Complaint sdata) throws SQLException {
		// TODO Auto-generated method stub
		
		System.out.print("inside add complaint details");
		//String sqlCommand = "Insert into tbl_complaint (Subject,description,`read`,complaint_status,complaint_type)values(' " + sdata.getSubject()+ " ',' " +sdata.getDescription()+ " ',"+sdata.getRead()+" ,' " +sdata.getComplaint_status() + "',' " +sdata.getComplaint_type()+ " ');";
		String sqlCommand="call sp_add_complaint(' " + sdata.getSubject()+ " ',' " +sdata.getDescription()+ " ',"+sdata.getRead()+" ,' " +sdata.getComplaint_status() + "',' " +sdata.getComplaint_type()+ " ');";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//display complaintdeatils

	public ArrayList<Complaint> GetComplaintDetails(Connection connection, String cmId) throws SQLException{
		ArrayList<Complaint>  allcm =new ArrayList<Complaint>();
 		try
 		{
 			
 			//String query ="select Subject,description,`read`,complaint_status,complaint_type from tbl_complaint";
 			String query="call sp_get_complaintdetails()";
 			System.out.println(query);
 			PreparedStatement ps = connection.prepareStatement(query);
 		
 			ResultSet rs = ps.executeQuery();
 			while(rs.next())
 			{
 				Complaint cm = new Complaint();
 				cm.setSubject(rs.getString("Subject"));
 				cm.setDescription(rs.getString("description"));
 				cm.setRead(rs.getInt("read"));
 				cm.setComplaint_status(rs.getString("complaint_status"));
 				cm.setComplaint_type(rs.getString("complaint_type"));
 				
 				
 				allcm.add(cm);
 			}
 			return allcm;
 		}
 		catch(Exception e)
 		{
 			throw e;
 		}
	}

	//insert notice
	public void addNoticeboarddeatils(Connection connection, Noticeboard sdata)throws SQLException {
		// TODO Auto-generated method stub
		

		System.out.print("inside add noticeboard details");
		//String sqlCommand = "Insert into tbl_noticeboard (notice_date,valid_until,notice_tile,description)values('" + sdata.getNotice_date()+ " ',' " +sdata.getValid_until()+ " ',' " +sdata.getNotice_tile()+ " ' ,' " +sdata.getDescription() + "');";
		String sqlCommand="call sp_add_notice(' " +sdata.getValid_until()+ " ',' " +sdata.getNotice_tile()+ " ',' " +sdata.getSubject()+ " ' ,' " +sdata.getDescription() + "',' " +sdata.getAttachment()+ " ');";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//display Noticeboarddetails

	public ArrayList<Noticeboard> GetNoticeboardDetails(Connection connection, String varNId)throws SQLException {
		// TODO Auto-generated method stub
		ArrayList<Noticeboard>  allnm =new ArrayList<Noticeboard>();
 		try
 		{
 			
 			//String query ="select notice_date,valid_until,notice_tile,description from tbl_noticeboard" ;
 			String query="call sp_get_noticeboarddetails()";
 			System.out.println(query);
 			PreparedStatement ps = connection.prepareStatement(query);
 		
 			ResultSet rs = ps.executeQuery();
 			while(rs.next())
 			{
 				Noticeboard nm = new Noticeboard();
 				
 				nm.setNotice_id(rs.getInt("notice_id"));
 				nm.setNotice_date(rs.getDate("notice_date"));
 				nm.setNotice_tile(rs.getString("notice_tile"));
 				nm.setSubject(rs.getString("subject"));
 				nm.setDescription(rs.getString("description"));
 				nm.setAttachment(rs.getString("attachment"));
 				
 				allnm.add(nm);
 			}
 			return allnm;
 		}
 		catch(Exception e)
 		{
 			throw e;
 		}
	}
	
	//insert meetingtrakingdetails

	public void addmeetingdeatils(Connection connection, meetingtraking sdata)throws SQLException {
		// TODO Auto-generated method stub
		
		System.out.print("inside add meeting details");
		//String sqlCommand = "Insert into tbl_meeting_tracking (meeting_date,title,points_discussed,conclusion)values('" + sdata.getMeeting_date()+ " ',' " +sdata.getTitle()+ " ',' " +sdata.getPoints_discussed()+ " ' ,' " +sdata.getConclusion() + "');";
		String sqlCommand="call sp_add_meeting('" + sdata.getMeeting_date()+ " ',' " +sdata.getTitle()+ " ',' " +sdata.getPoints_discussed()+ " ' ,' " +sdata.getConclusion() + "');";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//display meetingdetails

	public ArrayList<meetingtraking> GetmeetingDetails(Connection connection, String mid) throws Exception {
		// TODO Auto-generated method stub
		ArrayList<meetingtraking>  allmt =new ArrayList<meetingtraking>();
 		try
 		{
 			
 			//String query ="select meeting_date ,title, points_discussed ,conclusion from tbl_meeting_tracking";
 			String query="call sp_get_meetingdetails()";
 			System.out.println(query);
 			PreparedStatement ps = connection.prepareStatement(query);
 		
 			ResultSet rs = ps.executeQuery();
 			while(rs.next())
 			{
 				meetingtraking mt = new meetingtraking();
 				
 				mt.setMeeting_date(rs.getDate("meeting_date"));
 				mt.setTitle(rs.getString("title"));
 				mt.setPoints_discussed(rs.getString("points_discussed"));
 				mt.setConclusion(rs.getString("conclusion"));
 				
 				allmt.add(mt);
 			}
 			return allmt;
 		}
 		catch(Exception e)
 		{
 			throw e;
 		}
	}
	
	//insert achievement

	public void addAchievementdetails(Connection connection, Achievement sdata) throws SQLException{
		// TODO Auto-generated method stub
		
		System.out.print("inside add achievement details");
		//String sqlCommand = "Insert into tbl_achievment (achievment,description)values('" + sdata.getAchievment()+ " ',' " +sdata.getDescription()+ "');";
		String sqlCommand="call sp_add_achievement('" + sdata.getAchievment()+ " ',' " +sdata.getDescription()+ "');";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	//display achievement
	public ArrayList<Achievement> GetachievementDetails(Connection connection, String aid)throws SQLException {
		// TODO Auto-generated method stub
		ArrayList<Achievement>  allat =new ArrayList<Achievement>();
 		try
 		{
 			
 			//String query ="select achievment,description from tbl_achievment";
 			String query="call sp_get_achievementdetails()";
 			System.out.println(query);
 			PreparedStatement ps = connection.prepareStatement(query);
 		
 			ResultSet rs = ps.executeQuery();
 			while(rs.next())
 			{
 				Achievement at = new Achievement();
 				at.setAchievment(rs.getString("achievment"));
 				at.setDescription(rs.getString("description"));
 				
 				allat.add(at);
 			}
 			return allat;
 		}
 		catch(Exception e)
 		{
 			throw e;
 		}
	}

	//insert forum
	public void addforumdetails(Connection connection, Forum sdata) throws SQLException{
		// TODO Auto-generated method stub
		
		System.out.print("inside add forum details");
		//String sqlCommand = "Insert into tbl_forum (title,description,date)values ('" + sdata.getTitles()+ " ',' " +sdata.getDescription()+ "','" +sdata.getDate()+ "');";
		String sqlCommand="call sp_add_forum('" + sdata.getTitles()+ " ',' " +sdata.getDescription()+ "','" +sdata.getDate()+ "')";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//display forumdetails

		public ArrayList<Forum> Getforumdetails(Connection connection, String fid) throws Exception {
			// TODO Auto-generated method stub
			ArrayList<Forum>  allft =new ArrayList<Forum>();
	 		try
	 		{
	 			
	 			//String query ="select title,description,date from tbl_forum";
	 			String query="call sp_get_forumdetails()";
	 			System.out.println(query);
	 			PreparedStatement ps = connection.prepareStatement(query);
	 		
	 			ResultSet rs = ps.executeQuery();
	 			while(rs.next())
	 			{
	 				Forum ft = new Forum();
	 				ft.setTitles(rs.getString("title"));
	 				ft.setDescription(rs.getString("description"));
	 				ft.setDate(rs.getDate("date"));
	 				
	 				allft.add(ft);
	 			}
	 			return allft;
	 		}
	 		catch(Exception e)
	 		{
	 			throw e;
	 		}
		}
	
	//insert forumcomment

	public void addforumcommentdetails(Connection connection, forumcomment sdata)throws SQLException {
		// TODO Auto-generated method stub
		
		System.out.print("inside add forumcomment details");
		//String sqlCommand = "Insert into tbl_forum_comment (comment,date)values ('" + sdata.getComment()+ " ',' " +sdata.getDate()+ "');";
		String sqlCommand="call sp_add_forumcomment('" + sdata.getComment()+ " ',' " +sdata.getDate()+ "');";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//display forumcommentdetails
	

		public ArrayList<forumcomment> Getforumcommentdetails(Connection connection, String fcid) throws Exception {
			// TODO Auto-generated method stub
			ArrayList<forumcomment>  allft =new ArrayList<forumcomment>();
	 		try
	 		{
	 			
	 			//String query ="select comment,date from tbl_forum_comment";
	 			String query="call sp_get_forumcommentdetails()";
	 			System.out.println(query);
	 			PreparedStatement ps = connection.prepareStatement(query);
	 		
	 			ResultSet rs = ps.executeQuery();
	 			while(rs.next())
	 			{
	 				forumcomment fcm = new forumcomment();
	 				fcm.setComment(rs.getString("comment"));
	 				fcm.setDate(rs.getDate("date"));
	 				
	 				allft.add(fcm);
	 			}
	 			return allft;
	 		}
	 		catch(Exception e)
	 		{
	 			throw e;
	 		}
		}
		
		

	//insert maintenancecharge
	
	public void addMaintenanceChargedetails(Connection connection, MaintenanceCharge sdata)throws SQLException {
		// TODO Auto-generated method stub
		
		System.out.print("inside add maintenancecharge details");
		//String sqlCommand = "Insert into tbl_maintenance_charge(mnt_desc,mnt_baseamount,mnt_otherfund,mnt_sinkingamt,mnt_date,paid_date,status)values ('" + sdata.getMnt_desc()+ " '," +sdata.getMnt_baseamount()+ "," + sdata.getMnt_otheramount()+ " ," +sdata.getMnt_sinkingamt()+ ",'" + sdata.getMnt_date()+ " ',' " +sdata.getPaid_date()+ "','" + sdata.getStatus()+ " ');";
		String sqlCommand ="call sp_add_maintenancecharge(" + sdata.getMnt_id()+ ",'" + sdata.getMnt_desc()+ " '," +sdata.getMnt_baseamount()+ "," + sdata.getMnt_otheramount()+ " ," +sdata.getMnt_sinkingamt()+ "," + sdata.getSm_id()+ ",'" + sdata.getTo_date()+ " ',' " +sdata.getFrom_date()+ "','" + sdata.getStatus()+ " ');";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//display maintenancecharge

	public ArrayList<MaintenanceCharge> GetMaintenanceChargedetails(Connection connection, String mid) throws Exception {
		// TODO Auto-generated method stub
		ArrayList<MaintenanceCharge>  allat =new ArrayList<MaintenanceCharge>();
 		try
 		{
 			
 			//String query ="select mnt_desc,mnt_baseamount,mnt_otherfund,mnt_sinkingamt,mnt_date,paid_date,status from tbl_maintenance_charge";
 			String query="call sp_get_maintenancechargedetails()";
 			System.out.println(query);
 			PreparedStatement ps = connection.prepareStatement(query);
 		
 			ResultSet rs = ps.executeQuery();
 			while(rs.next())
 			{
 				MaintenanceCharge mc = new MaintenanceCharge();
 				
 				mc.setMnt_id(rs.getInt("mnt_id"));
 				mc.setMnt_desc(rs.getString("mnt_desc"));
 				/*mc.setMnt_baseamount(rs.getFloat("mnt_baseamount"));
 				mc.setMnt_otheramount(rs.getFloat("mnt_otherfund"));
 				mc.setMnt_sinkingamt(rs.getFloat("mnt_sinkingamt"));*/
 				mc.setTo_date(rs.getString("to_date"));
 				mc.setFrom_date(rs.getString("from_date"));
 				mc.setStatus(rs.getString("status"));
 				mc.setTotalamount(rs.getFloat("toalamount"));
 				mc.setSm_id(rs.getInt("sm_id"));
 				
 				allat.add(mc);
 			}
 			return allat;
 		}
 		catch(Exception e)
 		{
 			throw e;
 		}
	
		
	}

	//insert maintenanceamount
	
	public void addmaintenanceamountdetails(Connection connection, maintenanceamount sdata) throws SQLException{
		// TODO Auto-generated method stub
		
		System.out.print("inside add maintenanceamount details");
		//String sqlCommand = "Insert into tbl_mnt_amount(effective_to,effective_from,amount)values('" + sdata.getEffective_to()+ " ',' " +sdata.getEffective_from()+ "'," +sdata.getAmount()+ ");";
		String sqlCommand="call sp_add_maintenanceamount('" + sdata.getEffective_to()+ " ',' " +sdata.getEffective_from()+ "'," +sdata.getAmount()+ ");";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//display maintenanceamount

	public ArrayList<maintenanceamount> GetmaintenanceamountDetails(Connection connection, String maid) throws SQLException{
		// TODO Auto-generated method stub
		ArrayList<maintenanceamount>  allmat =new ArrayList<maintenanceamount>();
 		try
 		{
 			
 			//String query ="select effective_to,effective_from,amount from tbl_mnt_amount";
 			String query="call sp_get_maintenanceamountdetails()";
 			System.out.println(query);
 			PreparedStatement ps = connection.prepareStatement(query);
 		
 			ResultSet rs = ps.executeQuery();
 			while(rs.next())
 			{
 				maintenanceamount mamt = new maintenanceamount();
 				
 				mamt.setEffective_to(rs.getDate("effective_to"));
 				mamt.setEffective_from(rs.getDate("effective_from"));
 				mamt.setAmount(rs.getFloat("amount"));
 				
 				allmat.add(mamt);
 			}
 			return allmat;
 		}
 		catch(Exception e)
 		{
 			throw e;
 		}
	}
	//insert maintenance invoice
	
		public void addMaintenanceInvoicedetails(Connection connection, MaintenanceInvoice sdata) throws SQLException{
			// TODO Auto-generated method stub
			
			System.out.print("inside add MaintenanceInvoice details");
			//String sqlCommand = "Insert into tbl_mnt_amount(effective_to,effective_from,amount)values('" + sdata.getEffective_to()+ " ',' " +sdata.getEffective_from()+ "'," +sdata.getAmount()+ ");";
			String sqlCommand="call sp_add_maintenance_invoice('" + sdata.getInvoice_date()+ " ',' " +sdata.getPayment_mode()+ "',' " +sdata.getNote()+ " '," +sdata.getSm_id()+ "," +sdata.getMnt_id()+ ");";
			System.out.print(sqlCommand);
			Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
			try {
				boolean insert = ps.execute(sqlCommand);
				System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		//display Maintenance Invoice

		public ArrayList<MaintenanceInvoice> Getmaintenanceinvoicedetails(Connection connection, String maid) throws SQLException{
			// TODO Auto-generated method stub
			ArrayList<MaintenanceInvoice>  allmat =new ArrayList<MaintenanceInvoice>();
	 		try
	 		{
	 			String query="call sp_get_maintenance_invoice()";
	 			System.out.println(query);
	 			PreparedStatement ps = connection.prepareStatement(query);
	 		
	 			ResultSet rs = ps.executeQuery();
	 			while(rs.next())
	 			{
	 				MaintenanceInvoice mamt = new MaintenanceInvoice();
	 				
	 				mamt.setInvoice_date(rs.getString("invoice_date"));
	 				mamt.setPayment_mode(rs.getString("payment_mode"));
	 				mamt.setNote(rs.getString("note"));
	 				mamt.setSm_id(rs.getInt("sm_id"));
	 				mamt.setMnt_id(rs.getInt("mnt_id"));
	 				
	 				allmat.add(mamt);
	 			}
	 			return allmat;
	 		}
	 		catch(Exception e)
	 		{
	 			throw e;
	 		}
		}
	//insert penaltycharge

	public void addpenaltychargedetails(Connection connection, penaltycharge sdata) throws SQLException {
		// TODO Auto-generated method stub
		
		System.out.print("inside add penaltycharge details");
		//String sqlCommand = "Insert into tbl_penalty_charge(penalty_desc,penalty_over_duedate,penalty_over_due_days)values('" + sdata.getPenalty_desc()+ " ',' " +sdata.getPenalty_over_duedate()+ "'," +sdata.getPenalty_over_due_days()+ ");";
		String sqlCommand="call sp_add_panaltycharge('" + sdata.getPenalty_desc()+ " ',' " +sdata.getPenalty_over_duedate()+ "'," +sdata.getPenalty_over_due_days()+ "," +sdata.getSm_id()+ ");";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	//display penaltycharge

	public ArrayList<penaltycharge> GetpenaltychargeDetails(Connection connection, String pid)throws SQLException {
		// TODO Auto-generated method stub
		ArrayList<penaltycharge>  allmat =new ArrayList<penaltycharge>();
 		try
 		{
 			
 			//String query ="select penalty_desc,penalty_over_duedate,penalty_over_due_days from tbl_penalty_charge";
 			String query="call sp_get_penaltychargedetails()";
 			System.out.println(query);
 			PreparedStatement ps = connection.prepareStatement(query);
 		
 			ResultSet rs = ps.executeQuery();
 			while(rs.next())
 			{
 				penaltycharge pcmt = new penaltycharge();
 				
 				pcmt.setPenalty_desc(rs.getString("penalty_desc"));
 				pcmt.setPenalty_over_duedate(rs.getDate("penalty_over_duedate"));
 				pcmt.setPenalty_over_due_days(rs.getInt("penalty_over_due_days"));
 				pcmt.setSm_id(rs.getInt("sm_id"));
 				
 				allmat.add(pcmt);
 			}
 			return allmat;
 		}
 		catch(Exception e)
 		{
 			throw e;
 		}
	}
	
	public ArrayList<Registration> GetUserDetails(Connection connection) throws Exception
 	{
 		ArrayList<Registration>  UserDetails =new ArrayList<Registration>();
 		try
 		{
 			
 			
 			String query="call sp_get_userDetails()";
 			System.out.println(query);
 			PreparedStatement ps = connection.prepareStatement(query);
 			//ps.setString(1,uname);
 			ResultSet rs = ps.executeQuery();
 			while(rs.next())
 			{
 				Registration r = new Registration();
 				r.setFirstname(rs.getString("firstname"));
 				r.setLastname(rs.getString("lastname"));
 				r.setMobile_no(rs.getInt("mobile_no"));
 				r.setMembership(rs.getString("membership"));
 				r.setEmail(rs.getString("email"));
 				r.setFlat_no(rs.getString("flat_no"));
 				r.setPhase_name(rs.getString("phase_name"));
 				r.setSubphase(rs.getString("subphase"));
 				r.setFloor(rs.getInt("floor"));
 				UserDetails.add(r);
 				}
 			return UserDetails;
 		}
 		catch(Exception e)
 		{
 			throw e;
 		}
 }
	
	public ArrayList<Registration> GetRegisteredUserDetails(Connection connection) throws Exception
 	{
 		ArrayList<Registration> RegisteredUserDetails =new ArrayList<Registration>();
 		try
 		{
 			
 			String query="call sp_get_registeredUser()";
 			System.out.println(query);
 			PreparedStatement ps = connection.prepareStatement(query);
 			//ps.setString(1,uname);
 			ResultSet rs = ps.executeQuery();
 			while(rs.next())
 			{
 				Registration r = new Registration();
 				r.setFirstname(rs.getString("firstname"));
 				r.setLastname(rs.getString("lastname"));
 				r.setMobile_no(rs.getInt("mobile_no"));
 				r.setMembership(rs.getString("membership"));
 				r.setEmail(rs.getString("email"));
 				r.setFlat_no(rs.getString("flat_no"));
 				r.setPhase_name(rs.getString("phase_name"));
 				r.setSubphase(rs.getString("subphase"));
 				r.setFloor(rs.getInt("floor"));
 				r.setIs_admin(rs.getString("is_admin"));
 				RegisteredUserDetails.add(r);
 				}
 			return RegisteredUserDetails;
 		}
 		catch(Exception e)
 		{
 			throw e;
 		}
 }
	
	public ArrayList<Registration> GetRegisterDetails(Connection connection) throws Exception
	{
		ArrayList<Registration> RegisterDetails = new ArrayList<Registration>();
		try
		{
			
			String query ="SELECT distinct sm_id, firstname,lastname FROM tbl_society_member" ;
			PreparedStatement ps = connection.prepareStatement(query);
			
			System.out.println(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Registration owner= new Registration();
				owner.setOwner_id(rs.getInt("sm_id"));
				owner.setFirstname(rs.getString("firstname"));
				owner.setLastname(rs.getString("lastname"));
				RegisterDetails.add(owner );
				System.out.println(RegisterDetails.toString());
			}
			return RegisterDetails;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public void addRegisterDetails(Connection connection,Registration cdata) throws SQLException{
		System.out.print("inside registration details");
		String membership=cdata.getMembership();
		Integer owner_id=cdata.getOwner_id();
		String phase_name=cdata.getPhase_name();
		String subphase=cdata.getSubphase();
		String flat_no=cdata.getFlat_no();
		Integer floor=cdata.getFloor();
		String sqlCommand;
		if(membership.equals("Owner"))
		{
			owner_id = null;
	
		}
		else
		{
			phase_name = "empty";
			subphase=null;
			flat_no=null;
			floor = null;
			
		}
			sqlCommand = "Call sp_add_reg_details ( '"+phase_name+"','" + subphase+"','" +flat_no+"'," +floor+",'"+cdata.getFirstname()+"','" + cdata.getLastname()+"','" +cdata.getEmail()+"','" +cdata.getMobile_no()+"','" +membership+ "'," + owner_id +");";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			ArrayList<Boolean> insert=new ArrayList<Boolean>();
			 insert.add(ps.execute(sqlCommand));
			 System.out.println(insert);
			System.out.println("Result of insertflat>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public ArrayList<Profile> GetProfileDetails(Connection connection, int sm_id, String firstname) throws Exception
	{
		ArrayList<Profile> ProfileDetails = new ArrayList<Profile>();
		try
		{
			
			String query ="Call sp_get_profile_details ( '"+sm_id+"','" + firstname+"');";
			PreparedStatement ps = connection.prepareStatement(query);
			
			System.out.println(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Profile profile= new Profile();
				profile.setSm_id(rs.getInt("sm_id"));
				profile.setFirstname(rs.getString("firstname"));
				profile.setLastname(rs.getString("lastname"));
				profile.setAddress(rs.getString("address"));
				profile.setEmail(rs.getString("email"));
				profile.setMobile_no(rs.getInt("mobile_no"));
				profile.setDob(rs.getString("dob"));
				profile.setAge(rs.getInt("age"));
				profile.setGender(rs.getString("gender"));
				profile.setProfession(rs.getString("profession"));
				profile.setPancard_no(rs.getString("pancard_no"));
				profile.setUsername(rs.getString("username"));
				profile.setPassword(rs.getString("password"));
				profile.setIs_admin(rs.getString("is_admin"));
				ProfileDetails.add(profile);
				System.out.println(ProfileDetails.toString());
			}
			return ProfileDetails;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	
	public Registration GetLoginDetails(Connection connection,String username, String password) throws Exception
	{
		Registration user = new Registration();
		InetAddress ipAddr=null;
		try
		{
			//String uname = request.getParameter("uname");
			String query ="SELECT sm.sm_id,sm.firstname,sm.lastname,ld.is_admin FROM tbl_society_member sm join tbl_log_details ld on ld.sm_id=sm.sm_id where username = '" + username + "' and password = '" +  password + "';";
			PreparedStatement ps = connection.prepareStatement(query);
			//ps.setString(1,uname);
			System.out.println(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				//User user = new User();
				user.setSm_id(rs.getInt("sm_id"));
				user.setFirstname(rs.getString("firstname"));
				user.setLastname(rs.getString("lastname"));
				user.setIs_admin(rs.getString("is_admin"));
				//user.setMobile(rs.getString("mobile_no"));
				
			}
			try {
	            ipAddr = InetAddress.getLocalHost();
	            System.out.println(ipAddr.getHostAddress());
	        } 
			catch (UnknownHostException ex) {
	            ex.printStackTrace();
	        }
			String sqlCommand = "Call sp_add_log_details ( '"+user.getSm_id()+"','"+ipAddr+"');";
			System.out.print(sqlCommand);
			Statement pss = (Statement) connection.createStatement();
			try {
				boolean insert = pss.execute(sqlCommand);
				System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return user;
		}
		catch(Exception e)
		{
			throw e;
		}
}

	//insert Classified details
			public void addClassifiedDetails(Connection connection,Classified sdata) throws SQLException{
				System.out.print("inside add Classified details");
				//String sqlCommand = "Insert into tbl_classified (product_name,prod_description,prod_image,prod_cost,valid_upto) values (' " + sdata.getProductname()+ " ',' " +sdata.getProductdesc()+ " ', ' " +sdata.getProductimage()+ " ', " +sdata.getProductcost()+",' "+sdata.getValid_upto()+" ');";
				String sqlCommand="call sp_add_classified(' " + sdata.getProductname()+ " ',' " +sdata.getProductdesc()+ " ', ' " +sdata.getProductimage()+ " ', " +sdata.getProductcost()+","+sdata.getSm_id()+" ,' "+sdata.getValid_upto()+" ')";
				System.out.print(sqlCommand);
				Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
				try {
					boolean insert = ps.execute(sqlCommand);
					System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//display classified details
			   public ArrayList<Classified> GetClassifiedDetails(Connection connection,String cId) throws Exception
			 	{
			 		ArrayList<Classified>  allclass =new ArrayList<Classified>();
			 		try
			 		{
			 			
			 			//String query ="select product_name, prod_description, prod_image, prod_image, prod_cost,valid_upto from tbl_classified ";
			 			String query="call sp_get_classified()";
			 			System.out.println(query);
			 			PreparedStatement ps = connection.prepareStatement(query);
			 			//ps.setString(1,uname);
			 			ResultSet rs = ps.executeQuery();
			 			while(rs.next())
			 			{
			 				Classified c = new Classified();
			 				c.setProductname(rs.getString("product_name"));
			 				c.setProductdesc(rs.getString("prod_description"));
			 				c.setProductimage(rs.getString("prod_image"));
			 				c.setProductcost(rs.getDouble("prod_cost"));
			 				c.setValid_upto(rs.getString("valid_upto"));
			 				c.setFirstname(rs.getString("firstname"));
			 				c.setLastname(rs.getString("lastname"));
			 				c.setMobile_no(rs.getInt("mobile_no"));
			 				c.setSm_id(rs.getInt("sm_id"));
			 				allclass.add(c);
			 				}
			 			return allclass;
			 		}
			 		catch(Exception e)
			 		{
			 			throw e;
			 		}
			 }
			   
			 //insert Event details
				public void addEventDetails(Connection connection,Event sdata) throws SQLException{
					System.out.print("inside add Event details");
					//String sqlCommand = "Insert into tbl_event (event_type,event_name,event_date,event_time) values(' " + sdata.getEvent_type()+"',' " +sdata.getEvent_name()+"','" +sdata.getEvent_date()+"',' " + sdata.getEvent_time()+" ');";
					String sqlCommand="call sp_add_event('" + sdata.getTitle()+"','" +sdata.getType()+"','" +sdata.getStartsAt()+"','" + sdata.getEndsAt()+"')";
					System.out.print(sqlCommand);
					Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
					try {
						boolean insert = ps.execute(sqlCommand);
						System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				//display event details
				   public ArrayList<Event> GetEventDetails(Connection connection,String eId) throws Exception
				 	{
				 		ArrayList<Event>  allevent =new ArrayList<Event>();
				 		try
				 		{
				 			String query="call sp_get_event()";
				 			System.out.println(query);
				 			PreparedStatement ps = connection.prepareStatement(query);
				 			//ps.setString(1,uname);
				 			ResultSet rs = ps.executeQuery();
				 			while(rs.next())
				 			{
				 				Event e = new Event();
				 				e.setTitle(rs.getString("title"));
				 				e.setType(rs.getString("type"));
				 				e.setStartsAt(rs.getString("startsAt"));
				 				e.setEndsAt(rs.getString("endsAt"));
				 				e.setIs_approved(rs.getInt("is_approved"));
				 				
				 				allevent.add(e);
				 				}
				 			return allevent;
				 		}
				 		catch(Exception e)
				 		{
				 			throw e;
				 		}
				 }
				 //insert event expense details
					public void addEventExpenseDetails(Connection connection,EventExpense sdata) throws SQLException{
						System.out.print("inside add Event Expense details");
						//String sqlCommand = "Insert into tbl_event_expense (exp_desc,amount,amount_type,exp_date) values(' " + sdata.getEventdesc()+"', " +sdata.getAmount()+", ' " +sdata.getAmount_type()+"', ' " +sdata.getExpdate()+"');";
						String sqlCommand="call sp_add_event_expense(' " + sdata.getEventdesc()+"', " +sdata.getAmount()+", ' " +sdata.getAmount_type()+"', ' " +sdata.getExpdate()+"')";
						System.out.print(sqlCommand);
						Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
						try {
							boolean insert = ps.execute(sqlCommand);
							System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//display event expense details
					   public ArrayList<EventExpense>  GetEventExpenseDetails(Connection connection,String eeId) throws Exception
					 	{
					 		ArrayList<EventExpense>  allee =new ArrayList<EventExpense>();
					 		try
					 		{
					 			//String query ="select exp_desc,amount,amount_type,exp_date,exp_date from tbl_event_expense ";
					 			String query="call sp_get_event_expense()";
					 			System.out.println(query);
					 			PreparedStatement ps = connection.prepareStatement(query);
					 			//ps.setString(1,uname);
					 			ResultSet rs = ps.executeQuery();
					 			while(rs.next())
					 			{
					 				EventExpense ee = new EventExpense();
					 				ee.setEventdesc(rs.getString("exp_desc"));
					 				ee.setAmount(rs.getInt("amount"));
					 				ee.setAmount_type(rs.getString("amount_type"));
					 				ee.setExpdate(rs.getString("exp_date"));
					 				
					 				
					 				allee.add(ee);
					 				
					 			}
					 			return allee;
					 		}
					 		catch(Exception e)
					 		{
					 			throw e;
					 		}
					 }
			 //insert event income details
				public void addEventIncomeDetails(Connection connection,EventIncome sdata) throws SQLException{
					System.out.print("inside add EventIncome details");
					//String sqlCommand = "Insert into tbl_event_income (amount,date,amount_type) values(' " + sdata.getAmount()+"',' " +sdata.getDate()+"', ' " +sdata.getAmount_type()+"');";
					String sqlCommand ="call sp_add_event_income(' " + sdata.getAmount()+"',' " +sdata.getDate()+"', ' " +sdata.getAmount_type()+"')";
					System.out.print(sqlCommand);
					Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
						try {
								boolean insert = ps.execute(sqlCommand);
								System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						//display event income details
				public ArrayList<EventIncome>  GetEventIncomeDetails(Connection connection,String eiId) throws Exception
					{
						 ArrayList<EventIncome>  allee =new ArrayList<EventIncome>();
						 	try
						 	{
						 			//String query ="select amount,date,amount_type from tbl_event_income";
						 		String query ="call sp_get_event_income()";
						 			System.out.println(query);
						 			PreparedStatement ps = connection.prepareStatement(query);
						 			//ps.setString(1,uname);
						 			ResultSet rs = ps.executeQuery();
						 			while(rs.next())
						 			{
						 				EventIncome ei = new EventIncome();
						 				ei.setAmount(rs.getInt("amount"));
						 				ei.setDate(rs.getString("date"));
						 				ei.setAmount_type(rs.getString("amount_type"));
						 			allee.add(ei);
						 				
						 			}
						 			return allee;
						 		}
						 		catch(Exception e)
						 		{
						 			throw e;
						 		}
						 }
				 //insert Booking details
				public void addBookingDetails(Connection connection,Booking sdata) throws SQLException{
					System.out.print("inside add booking details");
					//String sqlCommand = "Insert into tbl_booking (description) values(' " + sdata.getDescription()+"');";
					String sqlCommand ="call sp_add_booking(' " + sdata.getDescription()+"')";		
					System.out.print(sqlCommand);
					Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
						try {
								boolean insert = ps.execute(sqlCommand);
								System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
				//display Booking details
				public ArrayList<Booking>  GetBookingDetails(Connection connection,String bId) throws Exception
					{
						 ArrayList<Booking>  allbooking =new ArrayList<Booking>();
						 	try
						 	{
						 			//String query ="select description from tbl_booking";
						 		String query ="call sp_get_booking()";
						 			System.out.println(query);
						 			PreparedStatement ps = connection.prepareStatement(query);
						 			//ps.setString(1,uname);
						 			ResultSet rs = ps.executeQuery();
						 			while(rs.next())
						 			{
						 				Booking b = new Booking();
						 				b.setDescription(rs.getString("description"));
						 				allbooking.add(b);
						 				
						 			}
						 			return allbooking;
						 		}
						 		catch(Exception e)
						 		{
						 			throw e;
						 		}
						 }
				//insert polling details
				public void addPolling(Connection connection,Polling sdata) throws SQLException{
					System.out.print("inside add Polling details");
					//String sqlCommand = "Insert into tbl_polling (polling_response) values( '" + sdata.getPolling_response()+"');";
					String sqlCommand ="call sp_add_polling('" + sdata.getPolling_response()+"')";
					System.out.print(sqlCommand);
					Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
						try {
								boolean insert = ps.execute(sqlCommand);
								System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
				//display polling details
				public ArrayList<Polling>  GetPollingDetails(Connection connection,String pId) throws Exception
					{
						 ArrayList<Polling>  allpolling =new ArrayList<Polling>();
						 	try
						 	{
						 		//String query ="select polling_response from tbl_polling";
						 		String query ="call sp_get_polling()";
						 			System.out.println(query);
						 			PreparedStatement ps = connection.prepareStatement(query);
						 			//ps.setString(1,uname);
						 			ResultSet rs = ps.executeQuery();
						 			while(rs.next())
						 			{
						 				Polling p = new Polling();
						 				p.setPolling_response(rs.getString("polling_response"));
						 				allpolling.add(p);
						 				
						 			}
						 			return allpolling;
						 		}
						 		catch(Exception e)
						 		{
						 			throw e;
						 		}
						 }
				//insert vendor details
				public void addVendor(Connection connection,Vendor sdata) throws SQLException{
					System.out.print("inside add Vendor details");
					//String sqlCommand ="Insert into tbl_vendor_details (vendor_name,contact_no,email_id,pancard_no,vendor_type,vendor_address,service_tax_regno,TDS,username,vendor_password) values(' " + sdata.getVendorname()+"',' " +sdata.getContact_no()+"', ' " +sdata.getEmail_id()+"', ' " +sdata.getPancardno()+"',' " +sdata.getVendortype()+"',' " +sdata.getVendoraddress()+"',' " +sdata.getServicetax()+"',' " +sdata.getTds()+"',' " +sdata.getUsername()+"',' " +sdata.getPassword()+"');";
					String sqlCommand ="call sp_add_vendor(' " + sdata.getVendorname()+"',' " +sdata.getContact_no()+"', ' " +sdata.getEmail_id()+"', ' " +sdata.getPancardno()+"',' " +sdata.getVendortype()+"',' " +sdata.getVendoraddress()+"')";
					System.out.print(sqlCommand);
					Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
						try {
								boolean insert = ps.execute(sqlCommand);
								System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
				//display vendor details
				public ArrayList<Vendor>  GetVendorDetails(Connection connection,String vId) throws Exception
					{
						 ArrayList<Vendor>  allvendor =new ArrayList<Vendor>();
						 	try
						 	{
						 		//String query ="select vendor_name,contact_no,email_id,pancard_no,vendor_type,vendor_address,service_tax_regno,TDS from tbl_vendor_details";
						 		String query ="call sp_get_vendor()";
						 			System.out.println(query);
						 			PreparedStatement ps = connection.prepareStatement(query);
						 			//ps.setString(1,uname);
						 			ResultSet rs = ps.executeQuery();
						 			while(rs.next())
						 			{
						 				Vendor v = new Vendor();
						 				v.setVendorname(rs.getString("vendor_name"));
						 				v.setContact_no(rs.getString("contact_no"));
						 				v.setEmail_id(rs.getString("email_id"));
						 				v.setPancardno(rs.getString("pancard_no"));
						 				v.setVendortype(rs.getString("vendor_type"));
						 				v.setVendoraddress(rs.getString("vendor_address"));
						 				
						 				allvendor.add(v);
						 				
						 			}
						 			return allvendor;
						 		}
						 		catch(Exception e)
						 		{
						 			throw e;
						 		}
						 }
				//insert  ServiceOrder details
				public void addServiceOrder(Connection connection,ServiceOrder sdata) throws SQLException{
					System.out.print("inside add ServiceOrder details");
					//String sqlCommand ="Insert into tbl_service_order (start_date,end_date,contract_desc) values(' " + sdata.getStartdate()+"',' " +sdata.getEnddate()+"', ' " +sdata.getContractdesc()+"');";
					String sqlCommand ="call sp_add_service_order(' " + sdata.getStartdate()+"',' " +sdata.getEnddate()+"',' " +sdata.getContract_doc()+"', ' " +sdata.getContractdesc()+"')";
					System.out.print(sqlCommand);
					Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
						try {
								boolean insert = ps.execute(sqlCommand);
								System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
				//display service order
				public ArrayList<ServiceOrder>  GetServiceOrderDetails(Connection connection,String soId) throws Exception
				{
					 ArrayList<ServiceOrder>  allservice =new ArrayList<ServiceOrder>();
					 	try
					 	{
					 			//String query ="select start_date,end_date,contract_desc from tbl_service_order";
					 		String query ="call sp_get_service_order()";
					 			System.out.println(query);
					 			PreparedStatement ps = connection.prepareStatement(query);
					 			//ps.setString(1,uname);
					 			ResultSet rs = ps.executeQuery();
					 			while(rs.next())
					 			{
					 				ServiceOrder so = new ServiceOrder();
					 				so.setStartdate(rs.getString("start_date"));
					 				so.setEnddate(rs.getString("end_date"));
					 				so.setContract_doc(rs.getString("contract_doc"));
					 				so.setContractdesc(rs.getString("contract_desc"));
					 				
					 				allservice.add(so);
					 				
					 			}
					 			return allservice;
					 		}
					 		catch(Exception e)
					 		{
					 			throw e;
					 		}
					 }
				//insert  BillPayment details
				public void addBillPayment(Connection connection,BillPayment sdata) throws SQLException{
					System.out.print("inside add BillPayment details");
					//String sqlCommand ="Insert into tbl_bill_payment (bill_date,amount,bill_status,bill_type,description,chequeno,bill_comment) values(' " + sdata.getBilldate()+"'," +sdata.getAmount()+", ' " +sdata.getBillstatus()+"',' " + sdata.getBilltype()+"',' " +sdata.getBilldesc()+"', ' " +sdata.getChequeno()+"',' " +sdata.getBillcomment()+"');";
					String sqlCommand ="call sp_add_bill_payment(' " + sdata.getBilldate()+"'," +sdata.getAmount()+", ' " +sdata.getBillstatus()+"',' " + sdata.getBilltype()+"',' " +sdata.getBilldesc()+"', ' " +sdata.getChequeno()+"',' " +sdata.getBillcomment()+"')";
					System.out.print(sqlCommand);
					Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
						try {
								boolean insert = ps.execute(sqlCommand);
								System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
				//display BillPayment order
				public ArrayList<BillPayment>  GetBillPaymentDetails(Connection connection,String bpId) throws Exception
				{
					 ArrayList<BillPayment>  allbill =new ArrayList<BillPayment>();
					 	try
					 	{
					 			//String query ="select bill_date,amount,bill_status,bill_type,description,chequeno,bill_comment from tbl_bill_payment";
					 		String query ="call sp_get_bill_payment()";
					 			System.out.println(query);
					 			PreparedStatement ps = connection.prepareStatement(query);
					 			//ps.setString(1,uname);
					 			ResultSet rs = ps.executeQuery();
					 			while(rs.next())
					 			{
					 				BillPayment bp = new BillPayment();
					 				bp.setBilldate(rs.getString("bill_date"));
					 				bp.setAmount(rs.getFloat("amount"));
					 				bp.setBillstatus(rs.getString("bill_status"));
					 				bp.setBilltype(rs.getString("bill_type"));
					 				bp.setBilldesc(rs.getString("description"));
					 				bp.setChequeno(rs.getString("chequeno"));
					 				bp.setBillcomment(rs.getString("bill_comment"));
					 				
					 				allbill.add(bp);
					 				
					 			}
					 			return allbill;
					 		}
					 		catch(Exception e)
					 		{
					 			throw e;
					 		}
					 }
				//insert  Vehicle Registration details
				public void addVehicleRegistration(Connection connection,VehicleRegistration sdata) throws SQLException{
					System.out.print("inside add VehicleRegistration details");
					//String sqlCommand ="Insert into tbl_vehicle_reg (vehicle_type,vehicle_no) values(' " + sdata.getVehicletype()+"',' " +sdata.getVehicleno()+" ');";
					String sqlCommand ="call sp_add_vehicle_reg(' " + sdata.getVehicletype()+"',' " +sdata.getVehicleno()+" ')";
					System.out.print(sqlCommand);
					Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
						try {
								boolean insert = ps.execute(sqlCommand);
								System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
				//display  Vehicle Registration order
				public ArrayList<VehicleRegistration>  GetVehicleDetails(Connection connection,String vrId) throws Exception
				{
					 ArrayList<VehicleRegistration>  allvehicle =new ArrayList<VehicleRegistration>();
					 	try
					 	{
					 			//String query ="select vehicle_type,vehicle_no from tbl_vehicle_reg";
					 		String query ="call sp_get_vehicle_reg()";
					 			System.out.println(query);
					 			PreparedStatement ps = connection.prepareStatement(query);
					 			//ps.setString(1,uname);
					 			ResultSet rs = ps.executeQuery();
					 			while(rs.next())
					 			{
					 				VehicleRegistration vr = new VehicleRegistration();
					 				vr.setVehicletype(rs.getString("vehicle_type"));
					 				vr.setVehicleno(rs.getString("vehicle_no"));
					 				
					 			allvehicle.add(vr);
					 			}
					 			return allvehicle;
					 		}
					 		catch(Exception e)
					 		{
					 			throw e;
					 		}
					 }
				//display  Society Rules order
				public ArrayList<SocietyRules>  GetSocietyDetails(Connection connection,String srId) throws Exception
				{
					 ArrayList<SocietyRules>  allrules =new ArrayList<SocietyRules>();
					 	try
					 	{
					 			
					 		String query ="select date,description from tbl_society_rules";
					 			
					 			System.out.println(query);
					 			PreparedStatement ps = connection.prepareStatement(query);
					 			//ps.setString(1,uname);
					 			ResultSet rs = ps.executeQuery();
					 			while(rs.next())
					 			{
					 				SocietyRules sr = new SocietyRules();
					 				sr.setDate(rs.getDate("date"));
					 				sr.setDescription(rs.getString("description"));
					 				
					 				allrules.add(sr);
					 			}
					 			return allrules;
					 		}
					 		catch(Exception e)
					 		{
					 			throw e;
					 		}
					 }
					// 
				public void ApproveEvent(Connection connection,Event sdata) throws SQLException{
					System.out.print("inside add Event details");
					//String sqlCommand = "Insert into tbl_event (event_type,event_name,event_date,event_time) values(' " + sdata.getEvent_type()+"',' " +sdata.getEvent_name()+"','" +sdata.getEvent_date()+"',' " + sdata.getEvent_time()+" ');";
					String sqlCommand="call sp_update_event('" + sdata.getTitle()+"','" +sdata.getType()+"','" +sdata.getStartsAt()+"','" + sdata.getEndsAt()+"')";
					System.out.print(sqlCommand);
					Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
					try {
						boolean insert = ps.execute(sqlCommand);
						System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			

	
}
