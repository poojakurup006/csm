package webService;


import java.io.IOException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.util.ArrayList;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.catalina.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import dto.*;

import model.ProjectManager;
import sun.misc.BASE64Decoder;


@Path("/WebService")
public class FeedService {
	
	// insert society member
	@POST
	@Path("/WriteSocietyMember")
	@Consumes("application/json")
	public void WriteSocietyMember(String message)
	{
		String feeds  = null;
		System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
		try 
		{
			ArrayList<User> feedData = null;
			
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			SocietyMember sdata = gson.fromJson(message, SocietyMember.class);
			ProjectManager projectManager= new ProjectManager();
			//System.out.println(sdata.getCost());
			 projectManager.WriteSocietyMember(sdata);
			System.out.println(sdata);
			//feeds = gson.toJson(feedData);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		//return feeds;
	}
	
	
	//display society member
	@GET
	@Path("/GetSocietyMemberDetails/")
	@Produces("application/json")
	
	public String GetSocietyMemberDetails(@PathParam("varsmId") String varsmId
		   )
	{
		String feeds  = null;
		
		try 
		{
			ArrayList<SocietyMember> allclass = null;
			ProjectManager projectManager= new ProjectManager();
			allclass = projectManager.GetSocietyMemberDetails(varsmId);
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(allclass));
			feeds = gson.toJson(allclass);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return feeds;
	}
	
	// insert family member
		@POST
		@Path("/WriteFamilyMember")
		@Consumes("application/json")
		public void WriteFamilyMember(String message)
		{
			String feeds  = null;
			System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
			try 
			{
				ArrayList<User> feedData = null;
				
				//StringBuffer sb = new StringBuffer();
				Gson gson = new Gson();
				MembersFamily sdata = gson.fromJson(message, MembersFamily.class);
				ProjectManager projectManager= new ProjectManager();
				//System.out.println(sdata.getCost());
				 projectManager.WriteFamilyMember(sdata);
				System.out.println(sdata);
				//feeds = gson.toJson(feedData);

			} catch (Exception e)
			{
				e.printStackTrace();
			}
			//return feeds;
		}
		
		//display society member
		@GET
		@Path("/GetFamilyMemberDetails/")
		@Produces("application/json")
		
		public String GetFamilyMemberDetails(@PathParam("varfmId") String varfmId
			   )
		{
			String feeds  = null;
			
			try 
			{
				ArrayList<MembersFamily> allfm = null;
				ProjectManager projectManager= new ProjectManager();
				allfm = projectManager.GetFamilyMemberDetails(varfmId);
				//StringBuffer sb = new StringBuffer();
				Gson gson = new Gson();
				System.out.println(gson.toJson(allfm));
				feeds = gson.toJson(allfm);

			} catch (Exception e)
			{
				e.printStackTrace();
			}
			return feeds;
		}
		
		// insert complaint
				@POST
				@Path("/WriteComplaint")
				@Consumes("application/json")
				public void WriteComplaint(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						Complaint sdata = gson.fromJson(message, Complaint.class);
						ProjectManager projectManager= new ProjectManager();
						//System.out.println(sdata.getCost());
						 projectManager.WriteComplaint(sdata);
						System.out.println(sdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}
		
		//display Complaint
				@GET
				@Path("/GetComplaintDetails/")
				@Produces("application/json")
				
				public String GetComplaintDetails(@PathParam("varcmId") String varcmId
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<Complaint> allfm = null;
						ProjectManager projectManager= new ProjectManager();
						allfm = projectManager.GetComplaintDetails(varcmId);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allfm));
						feeds = gson.toJson(allfm);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}
				
				
				// insert notie
				@POST
				@Path("/WriteNoticeboard")
				@Consumes("MULTIPART/FORM-DATA")
				public void WriteNoticeboard(
						@FormDataParam("attachment") InputStream uploadedInputStream,
						@FormDataParam("attachment") FormDataContentDisposition fileDetail,
						@FormDataParam("subject") String sub,
						@FormDataParam("valid_until") String validity,
						@FormDataParam("notice_tile") String title,
						@FormDataParam("description") String desc
						)
				{
					String feeds  = null;
					try 
					{
						String uploadedFileLocation = "E:/xampp/htdocs/CSM/CSMsociety/img/noticeFile/"+ fileDetail.getFileName();
						System.out.println("inside service" +sub);
						System.out.println("inside service" +validity);
						System.out.println("inside service" +title);
						System.out.println("inside service" +desc);
						Gson gson = new Gson();
						GsonBuilder gs = new GsonBuilder();
						gson=gs.setDateFormat("mm-dd-yyyy").create();
						
						Noticeboard classData= new Noticeboard();
						classData.setSubject(sub);
						classData.setValid_until(validity);
						classData.setAttachment(fileDetail.getFileName());
						classData.setNotice_tile(title);
						classData.setDescription(desc);
						
						System.out.println("image path "+classData.getAttachment());
						
						ProjectManager projectManager= new ProjectManager();
						 projectManager.WriteNoticeboard(classData);
						 System.out.println(classData);
						uploadFile(uploadedInputStream,uploadedFileLocation);
						
					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}
				
				
				
				//display Noticeboard
				@GET
				@Path("/GetNoticeboarddetails/")
				@Produces("application/json")
				
				public String GetNoticeboarddetails(@PathParam("varNId") String varNId
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<Noticeboard> allnm = null;
						ProjectManager projectManager= new ProjectManager();
						allnm = projectManager.GetNoticeboarddetails(varNId);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allnm));
						feeds = gson.toJson(allnm);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}
				
				
				// insert meeting
				@POST
				@Path("/Writemeetingtraking")
				@Consumes("application/json")
				public void Writemeetingtraking(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						
						GsonBuilder gs=new GsonBuilder();
						gson=gs.setDateFormat("mm-dd-yyyy").create();
						meetingtraking sdata = gson.fromJson(message, meetingtraking.class);
						
						ProjectManager projectManager= new ProjectManager();
						//System.out.println(sdata.getCost());
						 projectManager.Writemeetingtraking(sdata);
						System.out.println(sdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}
				
				
				//display meetingtraking
				@GET
				@Path("/Getmeetingtrakingdetails/")
				@Produces("application/json")
				
				public String Getmeetingtrakingdetails(@PathParam("mid") String mid
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<meetingtraking> allmt = null;
						ProjectManager projectManager= new ProjectManager();
						allmt = projectManager.Getmeetingtrakingdetails(mid);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allmt));
						feeds = gson.toJson(allmt);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}
				
				
				// insert Achievement
				@POST
				@Path("/WriteAchievement")
				@Consumes("application/json")
				public void WriteAchievement(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						Achievement sdata = gson.fromJson(message, Achievement.class);
						
						ProjectManager projectManager= new ProjectManager();
						//System.out.println(sdata.getCost());
						 projectManager.WriteAchievement(sdata);
						System.out.println(sdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}
				
				//display achievement
				@GET
				@Path("/Getachievementdetails/")
				@Produces("application/json")
				
				public String Getachievementdetails(@PathParam("aid") String aid
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<Achievement> allat = null;
						ProjectManager projectManager= new ProjectManager();
						allat = projectManager.Getachievementdetails(aid);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allat));
						feeds = gson.toJson(allat);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}
				
			//insert forum
				
				@POST
				@Path("/WriteForum")
				@Consumes("application/json")
				public void WriteForum(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						GsonBuilder gs=new GsonBuilder();
						gson=gs.setDateFormat("mm-dd-yyyy").create();
						Forum sdata = gson.fromJson(message, Forum.class);
						
						ProjectManager projectManager= new ProjectManager();
						//System.out.println(sdata.getCost());
						 projectManager.Writeforum(sdata);
						System.out.println(sdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}	
				
				//display forum
				@GET
				@Path("/Getforumdetails/")
				@Produces("application/json")
				
				public String Getforumdetails(@PathParam("fid") String fid
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<Forum> allat = null;
						ProjectManager projectManager= new ProjectManager();
						allat = projectManager.Getforumdetails(fid);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allat));
						feeds = gson.toJson(allat);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}
				
					//insert forumcomment
				
				@POST
				@Path("/Writeforumcomment")
				@Consumes("application/json")
				public void Writeforumcomment(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						GsonBuilder gs=new GsonBuilder();
						gson=gs.setDateFormat("mm-dd-yyyy").create();
						forumcomment sdata = gson.fromJson(message, forumcomment.class);
						
						ProjectManager projectManager= new ProjectManager();
						//System.out.println(sdata.getCost());
						 projectManager.Writeforumcomment(sdata);
						System.out.println(sdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}	
				
				//display forumcomment
				@GET
				@Path("/Getforumcommentdetails/")
				@Produces("application/json")
				
				public String Getforumcommentdetails(@PathParam("fcid") String fcid
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<forumcomment> allfct = null;
						ProjectManager projectManager= new ProjectManager();
						allfct = projectManager.Getforumcommentdetails(fcid);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allfct));
						feeds = gson.toJson(allfct);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}
				
		
					//insert maintenancecharge
				
				@POST
				@Path("/WriteMaintenanceCharge")
				@Consumes("application/json")
				public void WriteMaintenanceCharge(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						GsonBuilder gs=new GsonBuilder();
						gson=gs.setDateFormat("mm-dd-yyyy").create();
						MaintenanceCharge sdata = gson.fromJson(message, MaintenanceCharge.class);
						
						ProjectManager projectManager= new ProjectManager();
						//System.out.println(sdata.getCost());
						 projectManager.WriteMaintenanceCharge(sdata);
						System.out.println(sdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}	
				
				//display maintenancecharge
				@GET
				@Path("/GetMaintenanceChargedetails/")
				@Produces("application/json")
				
				public String GetMaintenanceChargedetails(@PathParam("mid") String mid
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<MaintenanceCharge> allmt = null;
						ProjectManager projectManager= new ProjectManager();
						allmt = projectManager.GetMaintenanceChargedetails(mid);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allmt));
						feeds = gson.toJson(allmt);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}	
				
				//insert maintenanceamount
				
				@POST
				@Path("/Writemaintenanceamount")
				@Consumes("application/json")
				public void Writemaintenanceamount(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						GsonBuilder gs=new GsonBuilder();
						gson=gs.setDateFormat("mm-dd-yyyy").create();
						maintenanceamount sdata = gson.fromJson(message, maintenanceamount.class);
						
						ProjectManager projectManager= new ProjectManager();
						//System.out.println(sdata.getCost());
						 projectManager.Writemaintenanceamount(sdata);
						System.out.println(sdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}	
				
				//display maintenanceamount
				@GET
				@Path("/Getmaintenanceamountdetails/")
				@Produces("application/json")
				
				public String Getmaintenanceamountdetails(@PathParam("maid") String maid
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<maintenanceamount> allmat = null;
						ProjectManager projectManager= new ProjectManager();
						allmat = projectManager.Getmaintenanceamountdetails(maid);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allmat));
						feeds = gson.toJson(allmat);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}	
				
					//insert maintenance invoice
				
				@POST
				@Path("/Writemaintenanceinvoice")
				@Consumes("application/json")
				public void Writemaintenanceinvoice(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						GsonBuilder gs=new GsonBuilder();
						gson=gs.setDateFormat("mm-dd-yyyy").create();
						MaintenanceInvoice sdata = gson.fromJson(message, MaintenanceInvoice.class);
						
						ProjectManager projectManager= new ProjectManager();
						//System.out.println(sdata.getCost());
						 projectManager.Writemaintenanceinvoice(sdata);
						System.out.println(sdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}
				//display maintenance invoice
				@GET
				@Path("/Getmaintenanceinvoicedetails/")
				@Produces("application/json")
				
				public String Getmaintenanceinvoicedetails(@PathParam("maid") String maid
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<MaintenanceInvoice> allmat = null;
						ProjectManager projectManager= new ProjectManager();
						allmat = projectManager.Getmaintenanceinvoicedetails(maid);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allmat));
						feeds = gson.toJson(allmat);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}	

				//insert penaltycharge
				
				@POST
				@Path("/Writepenaltycharge")
				@Consumes("application/json")
				public void Writepenaltycharge(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						GsonBuilder gs=new GsonBuilder();
						gson=gs.setDateFormat("mm-dd-yyyy").create();
						penaltycharge sdata = gson.fromJson(message, penaltycharge.class);
						
						ProjectManager projectManager= new ProjectManager();
						//System.out.println(sdata.getCost());
						 projectManager.Writepenaltycharge(sdata);
						System.out.println(sdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}	
				
				//display penaltycharge
				@GET
				@Path("/Getpenaltychargedetails/")
				@Produces("application/json")
				
				public String Getpenaltychargedetails(@PathParam("pid") String pid
					   )
				{
					String feeds  = null;
					
					try 
					{
						ArrayList<penaltycharge> allpcmt = null;
						ProjectManager projectManager= new ProjectManager();
						allpcmt = projectManager.Getpenaltychargedetails(pid);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(allpcmt));
						feeds = gson.toJson(allpcmt);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}

				@GET
				
				@Path("/GetUserDetails/")
				@Produces("application/json")
				public String GetUserDetails()
				{
					String feeds  = null;
					try 
					{
						//User user = null;
						ProjectManager projectManager= new ProjectManager();
						ArrayList <Registration> UserDetails = projectManager.GetUserDetails();
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(UserDetails));
						feeds = gson.toJson(UserDetails);
						/*Response res;*/
							
						
					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
					/*return Response.status(200)
							.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE").entity(feeds).build();*/
				}
				
				@GET
				
				@Path("/GetRegisteredUserDetails/")
				@Produces("application/json")
				public String GetRegisteredUserDetails()
				{
					String feeds  = null;
					try 
					{
						//User user = null;
						ProjectManager projectManager= new ProjectManager();
						ArrayList <Registration> RegisteredUserDetails = projectManager.GetRegisteredUserDetails();
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(RegisteredUserDetails));
						feeds = gson.toJson(RegisteredUserDetails);
						/*Response res;*/
							
						
					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
					/*return Response.status(200)
							.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE").entity(feeds).build();*/
				}
				
	
				@GET
				
				@Path("/GetRegisterDetails/")
				@Produces("application/json")
				public String GetRegisterDetails()
				{
					String feeds  = null;
					try 
					{
						//User user = null;
						ProjectManager projectManager= new ProjectManager();
						ArrayList <Registration> RegisterDetails = projectManager.GetRegisterDetails();
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(RegisterDetails));
						feeds = gson.toJson(RegisterDetails);
						/*Response res;*/
							
						
					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
					/*return Response.status(200)
							.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE").entity(feeds).build();*/
				}
				

				@POST
				@Path("/WriteRegisterData")
				@Consumes("application/json")
				public void WriteRegisterData(String message)
				{
					String feeds  = null;
					System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
					try 
					{
						//ArrayList<User> feedData = null;
						
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						Registration cdata = gson.fromJson(message, Registration.class);
						ProjectManager projectManager= new ProjectManager();
						System.out.println(cdata);
						 projectManager.WriteRegisterData(cdata);
						System.out.println(cdata);
						//feeds = gson.toJson(feedData);

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					//return feeds;
				}
				
				@GET
				
				@Path("/GetProfileDetails/{varSm_id}/{varFirstname}")
				@Produces("application/json")
				public String GetProfileDetails(
						@PathParam("varSm_id") int varSm_id,
						@PathParam("varFirstname") String varFirstname)
				{
					String feeds  = null;
					try 
					{
						//User user = null;
						ProjectManager projectManager= new ProjectManager();
						ArrayList <Profile> ProfileDetails = projectManager.GetProfileDetails(varSm_id,varFirstname);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(ProfileDetails));
						feeds = gson.toJson(ProfileDetails);
						/*Response res;*/
							
						
					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}
				

				
				@GET
				@Path("/GetLoginDetails/{varUsername}/{varPassword}")
				@Produces("application/json")
				public String GetLoginDetails(@PathParam("varUsername") String varUsername,
					    @PathParam("varPassword") String varPassword)
				{
					String feeds  = null;
					String decodedAuth = "";
					try 
					{
						byte[] bytes = null;
				        try {
				        	System.out.println(varPassword);
				            bytes = new BASE64Decoder().decodeBuffer(varPassword);
				        } catch (IOException e) {
				            // TODO Auto-generated catch block
				            e.printStackTrace();
				        }
				        decodedAuth = new String(bytes);
				        System.out.println(decodedAuth);
				        Registration user = null;
						ProjectManager projectManager= new ProjectManager();
						user = projectManager.GetLoginDetails(varUsername,decodedAuth);
						//StringBuffer sb = new StringBuffer();
						Gson gson = new Gson();
						System.out.println(gson.toJson(user));
						feeds = gson.toJson(user);
						

					} catch (Exception e)
					{
						e.printStackTrace();
					}
					return feeds;
				}
				
					// insert classified details
					@POST
					@Path("/WriteClassified")
					@Consumes("MULTIPART/FORM-DATA")
					public void WriteClassified(
							@FormDataParam("productimage") InputStream uploadedInputStream,
							@FormDataParam("productimage") FormDataContentDisposition fileDetail,
							@FormDataParam("productname") String pname,
							@FormDataParam("productdesc") String pdescription,
							@FormDataParam("productcost") String pcost ,
							@FormDataParam("valid_upto") String validdate,
							@FormDataParam("sm_id") int sm_id
							)
					{
						
						String feeds  = null;
						//System.out.println("Insert String2>>>>>>>>>>>>>>>" + pname + pdescription + pcost + validdate);
						try 
						{
							String uploadedFileLocation = "C:/xampp/htdocs/CSMsociety/img/classified/"+ fileDetail.getFileName();
							System.out.println("inside service" +pname);
							System.out.println("inside service" +pdescription);
							System.out.println("inside service" +pcost);
							System.out.println("inside service" +validdate);
							System.out.println("inside service" +sm_id);
							Gson gson = new Gson();
							GsonBuilder gs = new GsonBuilder();
							gson=gs.setDateFormat("mm-dd-yyyy").create();
							
							Classified classData= new Classified();
							classData.setProductname(pname);
							classData.setProductdesc(pdescription);
							classData.setProductimage(fileDetail.getFileName());
							classData.setProductcost(Double.parseDouble(pcost));
							classData.setValid_upto(validdate);
							classData.setSm_id(sm_id);
							
							System.out.println("image path "+classData.getProductimage());
							
							ProjectManager projectManager= new ProjectManager();
							 projectManager.WriteClassified(classData);
							 System.out.println(classData);
							uploadFile(uploadedInputStream,uploadedFileLocation);
							
							} catch (Exception e)
						{
							e.printStackTrace();
						}
						
					}

				
						//display classified details
					@GET
					@Path("/GetClassifiedDetails/")
					@Produces("application/json")
					
					public String GetClassifiedDetails(@PathParam("varcId") String varcId
						   )
					{
						String feeds  = null;
						
						try 
						{
							ArrayList<Classified> allclass = null;
							ProjectManager projectManager= new ProjectManager();
							allclass = projectManager.GetClassifiedDetails(varcId);
							//StringBuffer sb = new StringBuffer();
							Gson gson = new Gson();
							System.out.println(gson.toJson(allclass));
							feeds = gson.toJson(allclass);

						} catch (Exception e)
						{
							e.printStackTrace();
						}
						return feeds;
					}
					// insert event details
					@POST
					@Path("/WriteEventDetails")
					@Consumes("application/json")
					public void WriteEventDetails(String message)
					{
						String feeds  = null;
						System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
						try 
						{
							ArrayList<User> feedData = null;
							
							//StringBuffer sb = new StringBuffer();
							Gson gson = new Gson();
							GsonBuilder gs = new GsonBuilder();
							gson=gs.setDateFormat("mm-dd-yyyy").create();
							
							
							Event sdata = gson.fromJson(message, Event.class);
							ProjectManager projectManager= new ProjectManager();
							//System.out.println(sdata.getCost());
							 projectManager.WriteEventDetails(sdata);
							System.out.println(sdata);
							//feeds = gson.toJson(feedData);

						} catch (Exception e)
						{
							e.printStackTrace();
						}
						//return feeds;
					}
					//display event details
							@GET
							@Path("/GetEventDetails/")
							@Produces("application/json")
							
							public String GetEventDetails(@PathParam("vareId") String vareId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<Event> allevent = null;
									ProjectManager projectManager= new ProjectManager();
									allevent = projectManager.GetEventDetails(vareId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allevent));
									feeds = gson.toJson(allevent);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
							// insert event expense details
							@POST
							@Path("/WriteEventExpense")
							@Consumes("application/json")
							public void WriteEventExpense(String message)
							{
								String feeds  = null;
								System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
								try 
								{
									ArrayList<User> feedData = null;
									
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									EventExpense sdata = gson.fromJson(message, EventExpense.class);
									ProjectManager projectManager= new ProjectManager();
									//System.out.println(sdata.getCost());
									 projectManager.WriteEventExpense(sdata);
									System.out.println(sdata);
									//feeds = gson.toJson(feedData);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								//return feeds;
							}
							
							//display Event Expense details
							@GET
							@Path("/GetEventExpenseDetails/")
							@Produces("application/json")
							
							public String GetEventExpenseDetails(@PathParam("vareeId") String vareeId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<EventExpense> allee = null;
									ProjectManager projectManager= new ProjectManager();
									allee = projectManager.GetEventExpenseDetails(vareeId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allee));
									feeds = gson.toJson(allee);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
							// insert event income details
							@POST
							@Path("/WriteEventIncome")
							@Consumes("application/json")
							public void WriteEventIncome(String message)
							{
								String feeds  = null;
								System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
								try 
								{
									ArrayList<User> feedData = null;
									
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									EventIncome sdata = gson.fromJson(message, EventIncome.class);
									ProjectManager projectManager= new ProjectManager();
									//System.out.println(sdata.getCost());
									 projectManager.WriteEventIncome(sdata);
									System.out.println(sdata);
									//feeds = gson.toJson(feedData);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								//return feeds;
							}
							//display Event Income details
							@GET
							@Path("/GetEventIncomeDetails/")
							@Produces("application/json")
							
							public String GetEventIncomeDetails(@PathParam("vareiId") String vareiId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<EventIncome> allei = null;
									ProjectManager projectManager= new ProjectManager();
									allei = projectManager.GetEventIncomeDetails(vareiId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allei));
									feeds = gson.toJson(allei);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
							// insert Booking details
							@POST
							@Path("/WriteBooking")
							@Consumes("application/json")
							public void WriteBooking(String message)
							{
								String feeds  = null;
								System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
								try 
								{
									ArrayList<User> feedData = null;
									
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									Booking sdata = gson.fromJson(message, Booking.class);
									ProjectManager projectManager= new ProjectManager();
									//System.out.println(sdata.getCost());
									 projectManager.WriteBooking(sdata);
									System.out.println(sdata);
									//feeds = gson.toJson(feedData);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								//return feeds;
							}
							
					//display Booking details
							@GET
							@Path("/GetBookingDetails/")
							@Produces("application/json")
							
							public String GetBookingDetails(@PathParam("varbId") String varbId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<Booking> allbooking = null;
									ProjectManager projectManager= new ProjectManager();
									allbooking = projectManager.GetBookingDetails(varbId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allbooking));
									feeds = gson.toJson(allbooking);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
					// insert Polling details
							@POST
							@Path("/WritePolling")
							@Consumes("application/json")
							public void WritePolling(String message)
							{
								String feeds  = null;
								System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
								try 
								{
									ArrayList<User> feedData = null;
									
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									Polling sdata = gson.fromJson(message, Polling.class);
									ProjectManager projectManager= new ProjectManager();
									//System.out.println(sdata.getCost());
									 projectManager.WritePolling(sdata);
									System.out.println(sdata);
									//feeds = gson.toJson(feedData);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								//return feeds;
							}
						//display polling details
							@GET
							@Path("/GetPollingDetails/")
							@Produces("application/json")
							
							public String GetPollingDetails(@PathParam("varpId") String varpId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<Polling> allpolling = null;
									ProjectManager projectManager= new ProjectManager();
									allpolling = projectManager.GetPollingDetails(varpId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allpolling));
									feeds = gson.toJson(allpolling);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
							
							// insert vendor details
							@POST
							@Path("/WriteVendor")
							@Consumes("application/json")
							public void WriteVendor(String message)
							{
								String feeds  = null;
								System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
								try 
								{
									ArrayList<User> feedData = null;
									
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									Vendor sdata = gson.fromJson(message, Vendor.class);
									ProjectManager projectManager= new ProjectManager();
									//System.out.println(sdata.getCost());
									 projectManager.WriteVendor(sdata);
									System.out.println(sdata);
									//feeds = gson.toJson(feedData);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
							}
							//display vendor details
							@GET
							@Path("/GetVendorDetails/")
							@Produces("application/json")
							
							public String GetVendorDetails(@PathParam("varvId") String varvId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<Vendor> allvendor = null;
									ProjectManager projectManager= new ProjectManager();
									allvendor = projectManager.GetVendorDetails(varvId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allvendor));
									feeds = gson.toJson(allvendor);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
							
							// insert service order details
							@POST
							@Path("/WriteServiceOrder")
							@Consumes("MULTIPART/FORM-DATA")
							public void WriteServiceOrder(
									@FormDataParam("contract_doc") InputStream uploadedInputStream,
									@FormDataParam("contract_doc") FormDataContentDisposition fileDetail,
									@FormDataParam("startdate") String startdt,
									@FormDataParam("enddaste") String enddt,
									@FormDataParam("contractdesc") String condesc 
									)
							{
								String feeds  = null;
								//System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
								try 
								{
									String uploadedFileLocation = "C:/xampp/htdocs/sweethome/img/classified/"+ fileDetail.getFileName();
									System.out.println("inside service" +startdt);
									System.out.println("inside service" +enddt);
									System.out.println("inside service" +condesc);
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									
									ServiceOrder classData= new ServiceOrder();
									classData.setStartdate(startdt);
									classData.setEnddate(enddt);
									classData.setContract_doc(fileDetail.getFileName());
									classData.setContractdesc(condesc);
									
									System.out.println("image path "+classData.getContract_doc());
									
									ProjectManager projectManager= new ProjectManager();
									 projectManager.WriteServiceOrder(classData);
									 System.out.println(classData);
									uploadFile(uploadedInputStream,uploadedFileLocation);
									
								} catch (Exception e)
								{
									e.printStackTrace();
								}
							}
							

						/*	// insert classified details
							@POST
							@Path("/WriteClassified")
							@Consumes("MULTIPART/FORM-DATA")
							public void WriteClassified(
									@FormDataParam("productimage") InputStream uploadedInputStream,
									@FormDataParam("productimage") FormDataContentDisposition fileDetail,
									@FormDataParam("productname") String pname,
									@FormDataParam("productdesc") String pdescription,
									@FormDataParam("productcost") String pcost ,
									@FormDataParam("valid_upto") String validdate
									)
							{
								
								String feeds  = null;
								//System.out.println("Insert String2>>>>>>>>>>>>>>>" + pname + pdescription + pcost + validdate);
								try 
								{
									String uploadedFileLocation = "C:/xampp/htdocs/sweethome/img/classified/"+ fileDetail.getFileName();
									System.out.println("inside service" +pname);
									System.out.println("inside service" +pdescription);
									System.out.println("inside service" +pcost);
									System.out.println("inside service" +validdate);
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									
									Classified classData= new Classified();
									classData.setProductname(pname);
									classData.setProductdesc(pdescription);
									classData.setProductimage(fileDetail.getFileName());
									classData.setProductcost(Double.parseDouble(pcost));
									classData.setValid_upto(validdate);
									
									System.out.println("image path "+classData.getProductimage());
									
									ProjectManager projectManager= new ProjectManager();
									 projectManager.WriteClassified(classData);
									 System.out.println(classData);
									uploadFile(uploadedInputStream,uploadedFileLocation);
									
									} catch (Exception e)
								{
									e.printStackTrace();
								}
								
							}*/
							//display Service Order details
							@GET
							@Path("/GetServiceOrderDetails/")
							@Produces("application/json")
							
							public String GetServiceOrderDetails(@PathParam("varsoId") String varsoId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<ServiceOrder> allservice = null;
									ProjectManager projectManager= new ProjectManager();
									allservice = projectManager.GetServiceOrderDetails(varsoId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allservice));
									feeds = gson.toJson(allservice);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
							// insert Bill Payment  details
							@POST
							@Path("/WriteBillPayment")
							@Consumes("application/json")
							public void WriteBillPayment(String message)
							{
								String feeds  = null;
								System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
								try 
								{
									ArrayList<User> feedData = null;
									
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									BillPayment sdata = gson.fromJson(message, BillPayment.class);
									ProjectManager projectManager= new ProjectManager();
									projectManager.WriteBillPayment(sdata);
									System.out.println(sdata);
								

								} catch (Exception e)
								{
									e.printStackTrace();
								}
							}
							//display Bill Payment details
							@GET
							@Path("/GetBillPaymentDetails/")
							@Produces("application/json")
							
							public String GetBillPaymentDetails(@PathParam("varbpId") String varbpId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<BillPayment> allbill = null;
									ProjectManager projectManager= new ProjectManager();
									allbill = projectManager.GetBillPaymentDetails(varbpId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allbill));
									feeds = gson.toJson(allbill);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
							// insert Vehicle Registration  details
							@POST
							@Path("/WriteVehicleRegistration")
							@Consumes("application/json")
							public void WriteVehicleRegistration(String message)
							{
								String feeds  = null;
								System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
								try 
								{
									ArrayList<User> feedData = null;
									
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									VehicleRegistration sdata = gson.fromJson(message, VehicleRegistration.class);
									ProjectManager projectManager= new ProjectManager();
									projectManager.WriteVehicleRegistration(sdata);
									System.out.println(sdata);
								

								} catch (Exception e)
								{
									e.printStackTrace();
								}
							}
							//display Vehicle details
							@GET
							@Path("/GetVehicleDetails/")
							@Produces("application/json")
							
							public String GetVehicleDetails(@PathParam("varvrId") String varvrId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<VehicleRegistration> allvehicle = null;
									ProjectManager projectManager= new ProjectManager();
									allvehicle = projectManager.GetVehicleDetails(varvrId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allvehicle));
									feeds = gson.toJson(allvehicle);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
							//display Society rules details
							@GET
							@Path("/GetSocietyDetails/")
							@Produces("application/json")
							
							public String GetSocietyDetails(@PathParam("varsrId") String varsrId
								   )
							{
								String feeds  = null;
								
								try 
								{
									ArrayList<SocietyRules> allrules = null;
									ProjectManager projectManager= new ProjectManager();
									allrules = projectManager.GetSocietyDetails(varsrId);
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									System.out.println(gson.toJson(allrules));
									feeds = gson.toJson(allrules);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								return feeds;
							}
					// upload file function
							private void uploadFile(InputStream uploadedInputStream, String uploadedFileLocation) {
								
								System.out.println(uploadedFileLocation);
								writeToFile(uploadedInputStream, uploadedFileLocation);
								String output = "File uploaded to : " + uploadedFileLocation;
							}
						// write to file function	
							private void writeToFile(InputStream uploadedInputStream,
									String uploadedFileLocation) {

								try {
									OutputStream out = new FileOutputStream(new File(
											uploadedFileLocation));
									int read = 0;
									byte[] bytes = new byte[1024];

									out = new FileOutputStream(new File(uploadedFileLocation));
									while ((read = uploadedInputStream.read(bytes)) != -1) {
										out.write(bytes, 0, read);
									}
									
									out.flush();
									
									out.close();
									
								} catch (IOException e) {

									e.printStackTrace();
								}

							}
							

							// insert event details
							@POST
							@Path("/ApproveEvent")
							@Consumes("application/json")
							public void ApproveEvent(String message)
							{
								String feeds  = null;
								System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
								try 
								{
									ArrayList<User> feedData = null;
									
									//StringBuffer sb = new StringBuffer();
									Gson gson = new Gson();
									GsonBuilder gs = new GsonBuilder();
									gson=gs.setDateFormat("mm-dd-yyyy").create();
									
									
									Event sdata = gson.fromJson(message, Event.class);
									ProjectManager projectManager= new ProjectManager();
									//System.out.println(sdata.getCost());
									 projectManager.ApproveEvent(sdata);
									System.out.println(sdata);
									//feeds = gson.toJson(feedData);

								} catch (Exception e)
								{
									e.printStackTrace();
								}
								//return feeds;
							}
							//display event details
}
	


