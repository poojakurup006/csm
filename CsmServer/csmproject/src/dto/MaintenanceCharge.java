package dto;

import java.sql.Date;

public class MaintenanceCharge {
	
	private String mnt_desc;
	private Float mnt_baseamount;
	private Float mnt_otheramount;
	private Float mnt_sinkingamt;
	private String to_date;
	private String from_date;
	private String status;
	private Float Totalamount;
	private int mnt_id;
	private int sm_id;
	
	public String getMnt_desc() {
		return mnt_desc;
	}
	public void setMnt_desc(String mnt_desc) {
		this.mnt_desc = mnt_desc;
	}
	public Float getMnt_baseamount() {
		return mnt_baseamount;
	}
	public void setMnt_baseamount(Float mnt_baseamount) {
		this.mnt_baseamount = mnt_baseamount;
	}
	public Float getMnt_otheramount() {
		return mnt_otheramount;
	}
	public void setMnt_otheramount(Float mnt_otheramount) {
		this.mnt_otheramount = mnt_otheramount;
	}
	public Float getMnt_sinkingamt() {
		return mnt_sinkingamt;
	}
	public void setMnt_sinkingamt(Float mnt_sinkingamt) {
		this.mnt_sinkingamt = mnt_sinkingamt;
	}
	public String getTo_date() {
		return to_date;
	}
	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}
	public String getFrom_date() {
		return from_date;
	}
	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Float getTotalamount() {
		return Totalamount;
	}
	public void setTotalamount(Float totalamount) {
		Totalamount = totalamount;
	}
	public int getMnt_id() {
		return mnt_id;
	}
	public void setMnt_id(int mnt_id) {
		this.mnt_id = mnt_id;
	}
	public int getSm_id() {
		return sm_id;
	}
	public void setSm_id(int sm_id) {
		this.sm_id = sm_id;
	}
	
	
	
	

}
