package dto;

import java.sql.Date;

public class MembersFamily {
	
	private String firstname;
	private String lastname;
	private int age;
	private String profession;
	private String rel_with_owner;
	
	private String gender;
	private Date dob;
	private String pan_no;
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getProfession() {
		return profession;
	}
	public void setProfession(String profession) {
		this.profession = profession;
	}
	public String getRel_with_owner() {
		return rel_with_owner;
	}
	public void setRel_with_owner(String rel_with_owner) {
		this.rel_with_owner = rel_with_owner;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getPan_no() {
		return pan_no;
	}
	public void setPan_no(String pan_no) {
		this.pan_no = pan_no;
	}

}
