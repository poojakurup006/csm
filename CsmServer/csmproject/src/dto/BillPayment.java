package dto;

import java.sql.Date;

public class BillPayment {
	
	private String billdate;
	private float amount;
	private String billstatus;
	private String billtype;
	private String billdesc;
	private String chequeno;
	private String billcomment;
	
	public String getBilldate() {
		return billdate;
	}
	public void setBilldate(String billdate) {
		this.billdate = billdate;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getBillstatus() {
		return billstatus;
	}
	public void setBillstatus(String billstatus) {
		this.billstatus = billstatus;
	}
	public String getBilltype() {
		return billtype;
	}
	public void setBilltype(String billtype) {
		this.billtype = billtype;
	}
	public String getBilldesc() {
		return billdesc;
	}
	public void setBilldesc(String billdesc) {
		this.billdesc = billdesc;
	}
	public String getChequeno() {
		return chequeno;
	}
	public void setChequeno(String chequeno) {
		this.chequeno = chequeno;
	}
	public String getBillcomment() {
		return billcomment;
	}
	public void setBillcomment(String billcomment) {
		this.billcomment = billcomment;
	}
}
