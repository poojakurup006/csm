package dto;

public class Vendor {
	
	private String vendorname;
	private String contact_no;
	private String email_id;
	private String pancardno;
	private String vendortype;	
	private String vendoraddress;
		
	public String getVendorname() {
		return vendorname;
	}
	public void setVendorname(String vendorname) {
		this.vendorname = vendorname;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getPancardno() {
		return pancardno;
	}
	public void setPancardno(String pancardno) {
		this.pancardno = pancardno;
	}
	public String getVendortype() {
		return vendortype;
	}
	public void setVendortype(String vendortype) {
		this.vendortype = vendortype;
	}
	public String getVendoraddress() {
		return vendoraddress;
	}
	public void setVendoraddress(String vendoraddress) {
		this.vendoraddress = vendoraddress;
	}
	
	}

