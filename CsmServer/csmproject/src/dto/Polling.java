package dto;

public class Polling {
	
	private String polling_response;

	public String getPolling_response() {
		return polling_response;
	}

	public void setPolling_response(String polling_response) {
		this.polling_response = polling_response;
	}

}
