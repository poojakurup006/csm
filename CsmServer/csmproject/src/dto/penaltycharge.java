package dto;

import java.sql.Date;

public class penaltycharge {
	
	private String penalty_desc;
	private Date penalty_over_duedate;
	private int penalty_over_due_days;
	private int sm_id;
	
	
	public String getPenalty_desc() {
		return penalty_desc;
	}
	public void setPenalty_desc(String penalty_desc) {
		this.penalty_desc = penalty_desc;
	}
	public Date getPenalty_over_duedate() {
		return penalty_over_duedate;
	}
	public void setPenalty_over_duedate(Date penalty_over_duedate) {
		this.penalty_over_duedate = penalty_over_duedate;
	}
	public int getPenalty_over_due_days() {
		return penalty_over_due_days;
	}
	public void setPenalty_over_due_days(int penalty_over_due_days) {
		this.penalty_over_due_days = penalty_over_due_days;
	}
	public int getSm_id() {
		return sm_id;
	}
	public void setSm_id(int sm_id) {
		this.sm_id = sm_id;
	}
	
	

}
