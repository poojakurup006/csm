package dto;

import java.sql.Date;

public class maintenanceamount {
	
	private Date effective_to;
	private Date effective_from;
	private Float amount;
	
	public Date getEffective_to() {
		return effective_to;
	}
	public void setEffective_to(Date effective_to) {
		this.effective_to = effective_to;
	}
	public Date getEffective_from() {
		return effective_from;
	}
	public void setEffective_from(Date effective_from) {
		this.effective_from = effective_from;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	
	

}
