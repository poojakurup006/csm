package dto;

import java.text.DateFormat;
import java.text.ParseException;


public class Classified {
	private String productname;
	private String productdesc;
	private String productimage;
	private Double productcost;
	private String valid_upto;
	private String firstname;
	private String lastname;
	private int mobile_no;
	private int sm_id;
	
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getProductdesc() {
		return productdesc;
	}
	public void setProductdesc(String productdesc) {
		this.productdesc = productdesc;
	}
	public String getProductimage() {
		return productimage;
	}
	public void setProductimage(String productimage) {
		this.productimage = productimage.trim();
	}
	public Double getProductcost() {
		return productcost;
	}
	public void setProductcost(Double productcost) {
		this.productcost = productcost;
	}
	public String getValid_upto() {
		return valid_upto;
	}
	public void setValid_upto(String valid_upto)  {
		this.valid_upto = valid_upto;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(int mobile_no) {
		this.mobile_no = mobile_no;
	}
	public int getSm_id() {
		return sm_id;
	}
	public void setSm_id(int sm_id) {
		this.sm_id = sm_id;
	}
	
}
