package dto;

import java.sql.Date;

public class meetingtraking {
	
	private Date meeting_date;
	private String title;
	private String points_discussed;
	private String conclusion;
	public Date getMeeting_date() {
		return meeting_date;
	}
	public void setMeeting_date(Date meeting_date) {
		this.meeting_date = meeting_date;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPoints_discussed() {
		return points_discussed;
	}
	public void setPoints_discussed(String points_discussed) {
		this.points_discussed = points_discussed;
	}
	public String getConclusion() {
		return conclusion;
	}
	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}
	
	
	

}
