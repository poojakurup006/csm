package dto;

public class MaintenanceInvoice {
	private String invoice_date;
	private String payment_mode;
	private String note;
	private int sm_id;
	private int mnt_id;
	
	public String getInvoice_date() {
		return invoice_date;
	}
	public void setInvoice_date(String invoice_date) {
		this.invoice_date = invoice_date;
	}
	public String getPayment_mode() {
		return payment_mode;
	}
	public void setPayment_mode(String payment_mode) {
		this.payment_mode = payment_mode;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getSm_id() {
		return sm_id;
	}
	public void setSm_id(int sm_id) {
		this.sm_id = sm_id;
	}
	public int getMnt_id() {
		return mnt_id;
	}
	public void setMnt_id(int mnt_id) {
		this.mnt_id = mnt_id;
	}
}
