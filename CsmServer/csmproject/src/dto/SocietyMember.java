package dto;

import java.sql.Blob;

public class SocietyMember {
	
	private String firstname;
	private String lastname;
	private String address;
	private String email;
	private int mobile_no;
	private int no_of_fm_mem;
	private Boolean Is_tenant;
	//private Blob image;
	private String username;
	private String password;
	 
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(int mobile_no) {
		this.mobile_no = mobile_no;
	}
	public int getNo_of_fm_mem() {
		return no_of_fm_mem;
	}
	public void setNo_of_fm_mem(int no_of_fm_mem) {
		this.no_of_fm_mem = no_of_fm_mem;
	}
	public Boolean getIs_tenant() {
		return Is_tenant;
	}
	public void setIs_tenant(Boolean is_tenant) {
		Is_tenant = is_tenant;
	}
	/*public Blob getImage() {
		return image;
	}
	public void setImage(Blob image) {
		this.image = image;
	}*/
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}
