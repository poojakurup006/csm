package dto;

import java.sql.Date;
import java.sql.Time;

public class Event {
	
	private String title;
	private String type;
	private String startsAt;
	private String endsAt;
	private int is_approved;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStartsAt() {
		return startsAt;
	}
	public void setStartsAt(String startsAt) {
		this.startsAt = startsAt;
	}
	public String getEndsAt() {
		return endsAt;
	}
	public void setEndsAt(String endsAt) {
		this.endsAt = endsAt;
	}
	public int getIs_approved() {
		return is_approved;
	}
	public void setIs_approved(int is_approved) {
		this.is_approved = is_approved;
	}
	
}
