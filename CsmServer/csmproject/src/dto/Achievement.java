package dto;

public class Achievement {
	
	private String achievment;
	private String description;
	
	public String getAchievment() {
		return achievment;
	}
	public void setAchievment(String achievment) {
		this.achievment = achievment;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}
