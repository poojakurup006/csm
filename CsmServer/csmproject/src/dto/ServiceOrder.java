package dto;

import java.sql.Date;

public class ServiceOrder {
	
	private String startdate;
	private String enddate;
	private String contract_doc;
	private String contractdesc;
	
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getContract_doc() {
		return contract_doc;
	}
	public void setContract_doc(String contract_doc) {
		this.contract_doc = contract_doc;
	}
	public String getContractdesc() {
		return contractdesc;
	}
	public void setContractdesc(String contrachdesc) {
		this.contractdesc = contrachdesc;
	}
	
	

}
