/*
SQLyog Community v12.2.1 (64 bit)
MySQL - 5.5.48 : Database - csm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`csm` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `csm`;

/*Table structure for table `tbl_achievment` */

DROP TABLE IF EXISTS `tbl_achievment`;

CREATE TABLE `tbl_achievment` (
  `ach_id` int(11) NOT NULL AUTO_INCREMENT,
  `achievment` varchar(20) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `fm_id` int(11) DEFAULT NULL,
  `achievment_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ach_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_achievment` */

/*Table structure for table `tbl_amenity` */

DROP TABLE IF EXISTS `tbl_amenity`;

CREATE TABLE `tbl_amenity` (
  `amenity_id` int(11) NOT NULL AUTO_INCREMENT,
  `amenity_name` varchar(20) NOT NULL,
  `society_id` int(11) NOT NULL,
  PRIMARY KEY (`amenity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_amenity` */

/*Table structure for table `tbl_bill_payment` */

DROP TABLE IF EXISTS `tbl_bill_payment`;

CREATE TABLE `tbl_bill_payment` (
  `bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `amount` decimal(18,2) NOT NULL,
  `bill_status` varchar(10) NOT NULL,
  `bill_type` varchar(10) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `cm_id` int(11) DEFAULT NULL,
  `chequeno` varchar(15) DEFAULT NULL,
  `bill_comment` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`bill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_bill_payment` */

insert  into `tbl_bill_payment`(`bill_id`,`bill_date`,`amount`,`bill_status`,`bill_type`,`description`,`service_id`,`cm_id`,`chequeno`,`bill_comment`) values 
(2,'2016-03-11','445.00',' yd',' gfgdf',' gfghfg',NULL,NULL,' fgfdghf',' hfgh'),
(3,'2016-03-10','2000.00',' paid',' cash',' paid',NULL,NULL,' --',' All paid');

/*Table structure for table `tbl_booking` */

DROP TABLE IF EXISTS `tbl_booking`;

CREATE TABLE `tbl_booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(30) DEFAULT NULL,
  `amenity_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_booking` */

/*Table structure for table `tbl_classified` */

DROP TABLE IF EXISTS `tbl_classified`;

CREATE TABLE `tbl_classified` (
  `classified_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(15) NOT NULL,
  `prod_description` varchar(50) DEFAULT NULL,
  `prod_image` varchar(500) DEFAULT NULL,
  `prod_cost` decimal(18,2) NOT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `valid_upto` date DEFAULT NULL,
  PRIMARY KEY (`classified_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_classified` */
insert  into `tbl_classified`(`classified_id`,`product_name`,`prod_description`,`prod_image`,`prod_cost`,`sm_id`,`valid_upto`) values 
(2,' lamp ',' light lamp ',' lamp.jpg ','123.00',7,'2016-03-12'),
(3,' card ',' home made card ',' card.jpg ','50.00',8,'2016-03-10'),
(4,' candle ',' colourful candles ',' candles.jpg ','234.00',9,'2016-03-30'),
(6,' bottle ',' home made colourful bottles ',' bottle.jpg ','250.00',9,'2016-03-25'),
(7,' bag ',' light weight bags ',' bag.jpg ','100.00',7,'2016-03-31'),
(8,' stand ',' mobile stand ',' mobilestand.jpg ','100.00',7,'2016-03-30'),
(9,' mobile ',' sgha ',' mobile.jpg ','10000.00',NULL,'2016-03-09');

/*Table structure for table `tbl_cm_role` */

DROP TABLE IF EXISTS `tbl_cm_role`;

CREATE TABLE `tbl_cm_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  `responsible_for` varchar(100) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_cm_role` */

/*Table structure for table `tbl_committee_member` */

DROP TABLE IF EXISTS `tbl_committee_member`;

CREATE TABLE `tbl_committee_member` (
  `cm_id` int(11) NOT NULL AUTO_INCREMENT,
  `sm_id` int(11) NOT NULL,
  `duration` varchar(15) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`cm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_committee_member` */

/*Table structure for table `tbl_complaint` */

DROP TABLE IF EXISTS `tbl_complaint`;

CREATE TABLE `tbl_complaint` (
  `complaint_id` int(11) NOT NULL AUTO_INCREMENT,
  `sm_id` int(11) DEFAULT NULL,
  `subject` varchar(15) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `read` varchar(10) DEFAULT NULL,
  `complaint_status` varchar(10) DEFAULT NULL,
  `cm_id` int(11) DEFAULT NULL,
  `complaint_type` varchar(20) NOT NULL,
  `complaint_date` date DEFAULT NULL,
  PRIMARY KEY (`complaint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_complaint` */

/*Table structure for table `tbl_event` */

DROP TABLE IF EXISTS `tbl_event`;

CREATE TABLE `tbl_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(10) NOT NULL,
  `event_name` varchar(15) NOT NULL,
  `event_date` date DEFAULT NULL,
  `event_time` varchar(30) DEFAULT NULL,
  `cm_id` int(11) DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_event` */

insert  into `tbl_event`(`event_id`,`event_type`,`event_name`,`event_date`,`event_time`,`cm_id`,`booking_id`) values 
(1,' Society',' dhcd','2016-03-01',' 4:30 PM ',NULL,NULL),
(2,' Personal',' Holi','2016-03-12',' 12:15 AM ',NULL,NULL);

/*Table structure for table `tbl_event_expense` */

DROP TABLE IF EXISTS `tbl_event_expense`;

CREATE TABLE `tbl_event_expense` (
  `exp_id` int(11) NOT NULL AUTO_INCREMENT,
  `exp_desc` varchar(50) DEFAULT NULL,
  `amount` decimal(18,2) NOT NULL,
  `amount_type` varchar(20) DEFAULT NULL,
  `exp_date` date NOT NULL,
  `cm_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`exp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_event_expense` */

insert  into `tbl_event_expense`(`exp_id`,`exp_desc`,`amount`,`amount_type`,`exp_date`,`cm_id`,`event_id`) values 
(1,' diwali','1000.00',' for lights','2016-03-16',NULL,NULL);

/*Table structure for table `tbl_event_income` */

DROP TABLE IF EXISTS `tbl_event_income`;

CREATE TABLE `tbl_event_income` (
  `ei_id` int(11) NOT NULL AUTO_INCREMENT,
  `sm_id` int(11) DEFAULT NULL,
  `amount` decimal(18,2) NOT NULL,
  `date` date NOT NULL,
  `amount_type` varchar(10) NOT NULL,
  `cm_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ei_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_event_income` */

/*Table structure for table `tbl_flat` */

DROP TABLE IF EXISTS `tbl_flat`;

CREATE TABLE `tbl_flat` (
  `flat_id` int(11) NOT NULL AUTO_INCREMENT,
  `flat_type` varchar(7) DEFAULT NULL,
  `phase_name` varchar(10) DEFAULT NULL,
  `subphase` varchar(10) DEFAULT NULL,
  `flat_no` varchar(5) DEFAULT NULL,
  `floor` int(11) DEFAULT NULL,
  PRIMARY KEY (`flat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_flat` */

insert  into `tbl_flat`(`flat_id`,`flat_type`,`phase_name`,`subphase`,`flat_no`,`floor`) values 
(1,NULL,'Alpha','A','111',11);

/*Table structure for table `tbl_forum` */

DROP TABLE IF EXISTS `tbl_forum`;

CREATE TABLE `tbl_forum` (
  `forum_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `date` date NOT NULL,
  `attachment` varbinary(50) DEFAULT NULL,
  `sm_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`forum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_forum` */

/*Table structure for table `tbl_forum_comment` */

DROP TABLE IF EXISTS `tbl_forum_comment`;

CREATE TABLE `tbl_forum_comment` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) DEFAULT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `date` date NOT NULL,
  `attachment` varbinary(50) DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_forum_comment` */

/*Table structure for table `tbl_log_details` */

DROP TABLE IF EXISTS `tbl_log_details`;

CREATE TABLE `tbl_log_details` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_admin` varchar(10) DEFAULT NULL,
  `current_signin_ip` varchar(100) DEFAULT NULL,
  `previous_sigin_at` datetime DEFAULT NULL,
  `previous_sigin_ip` varchar(100) DEFAULT NULL,
  `password_change_date` datetime DEFAULT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `current_sigin_at` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_log_details` */

insert  into `tbl_log_details`(`log_id`,`is_admin`,`current_signin_ip`,`previous_sigin_at`,`previous_sigin_ip`,`password_change_date`,`sm_id`,`current_sigin_at`) values 
(1,'admin','Pooja/127.0.0.1','2016-03-30 00:00:00','Pooja/127.0.0.1',NULL,1,'2016-03-30 00:00:00'),
(2,'member','Pooja/127.0.0.1','2016-03-30 00:00:00','Pooja/127.0.0.1',NULL,2,'2016-03-30 00:00:00');

/*Table structure for table `tbl_maintenance_charge` */

DROP TABLE IF EXISTS `tbl_maintenance_charge`;

CREATE TABLE `tbl_maintenance_charge` (
  `mnt_id` int(11) NOT NULL AUTO_INCREMENT,
  `mnt_desc` varchar(50) DEFAULT NULL,
  `mnt_baseamount` decimal(18,2) DEFAULT NULL,
  `mnt_otherfund` decimal(18,2) DEFAULT NULL,
  `mnt_sinkingamt` decimal(18,2) DEFAULT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `cm_id` int(11) DEFAULT NULL,
  `to_date` date NOT NULL,
  `from_date` date DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `timeStamp` datetime DEFAULT NULL,
  `mnt_amt_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`mnt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_maintenance_charge` */

insert  into `tbl_maintenance_charge`(`mnt_id`,`mnt_desc`,`mnt_baseamount`,`mnt_otherfund`,`mnt_sinkingamt`,`sm_id`,`cm_id`,`to_date`,`from_date`,`status`,`timeStamp`,`mnt_amt_id`) values 
(1,'dsfsd ','33.00','33.00','33.00',NULL,NULL,'2016-03-04','2016-03-19','rewrf ',NULL,NULL),
(2,'fdfd ','33.00','33.00','33.00',NULL,NULL,'2016-03-02','2016-03-04','fds ',NULL,NULL),
(3,'fdsf ','64.00','53.00','534.00',NULL,NULL,'2016-03-17','2016-03-12','drs ',NULL,NULL),
(4,'main ','100.00','100.00','100.00',NULL,NULL,'2016-03-05','2016-03-26','paid ',NULL,NULL);

/*Table structure for table `tbl_maintenence_invoice` */

DROP TABLE IF EXISTS `tbl_maintenence_invoice`;

CREATE TABLE `tbl_maintenence_invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_date` date DEFAULT NULL,
  `payment_mode` varchar(15) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `mnt_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_maintenence_invoice` */

/*Table structure for table `tbl_meeting_tracking` */

DROP TABLE IF EXISTS `tbl_meeting_tracking`;

CREATE TABLE `tbl_meeting_tracking` (
  `meeting_id` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_date` date DEFAULT NULL,
  `title` varchar(30) DEFAULT NULL,
  `conclusion` varchar(80) DEFAULT NULL,
  `cm_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`meeting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_meeting_tracking` */

/*Table structure for table `tbl_members_family` */

DROP TABLE IF EXISTS `tbl_members_family`;

CREATE TABLE `tbl_members_family` (
  `fm_id` int(11) NOT NULL AUTO_INCREMENT,
  `sm_id` int(11) DEFAULT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `profession` varchar(50) DEFAULT NULL,
  `rel_with_owner` varchar(10) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `pancard_no` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`fm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_members_family` */

insert  into `tbl_members_family`(`fm_id`,`sm_id`,`firstname`,`lastname`,`age`,`profession`,`rel_with_owner`,`gender`,`dob`,`pancard_no`) values 
(1,1,'Pooja','Kurup',NULL,NULL,NULL,NULL,NULL,NULL),
(2,2,'Jyoti','Jagtap',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tbl_mnt_amount` */

DROP TABLE IF EXISTS `tbl_mnt_amount`;

CREATE TABLE `tbl_mnt_amount` (
  `mnt_amt_id` int(11) NOT NULL AUTO_INCREMENT,
  `effective_to` date DEFAULT NULL,
  `effective_from` date DEFAULT NULL,
  `amount` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`mnt_amt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_mnt_amount` */

/*Table structure for table `tbl_mom` */

DROP TABLE IF EXISTS `tbl_mom`;

CREATE TABLE `tbl_mom` (
  `mom_id` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_id` int(11) DEFAULT NULL,
  `points_discussed` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`mom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_mom` */

/*Table structure for table `tbl_noticeboard` */

DROP TABLE IF EXISTS `tbl_noticeboard`;

CREATE TABLE `tbl_noticeboard` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT,
  `notice_date` date NOT NULL,
  `valid_until` date NOT NULL,
  `notice_tile` varchar(15) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `attachment` varchar(200) DEFAULT NULL,
  `cm_id` int(11) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_noticeboard` */

insert  into `tbl_noticeboard`(`notice_id`,`notice_date`,`valid_until`,`notice_tile`,`description`,`attachment`,`cm_id`,`subject`) values 
(1,'2016-03-28','2016-03-09',' nfcsdonfoijoih',' hiujoijoijokio',' emma-300.jpg ',NULL,' uihoijiojioj ');

/*Table structure for table `tbl_parking` */

DROP TABLE IF EXISTS `tbl_parking`;

CREATE TABLE `tbl_parking` (
  `parking_id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_no` varchar(5) DEFAULT NULL,
  `flat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`parking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_parking` */

/*Table structure for table `tbl_penalty_charge` */

DROP TABLE IF EXISTS `tbl_penalty_charge`;

CREATE TABLE `tbl_penalty_charge` (
  `penalty_id` int(11) NOT NULL AUTO_INCREMENT,
  `penalty_desc` varchar(50) DEFAULT NULL,
  `penalty_over_duedate` date DEFAULT NULL,
  `penalty_over_due_days` int(11) DEFAULT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `cm_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`penalty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_penalty_charge` */

/*Table structure for table `tbl_polling` */

DROP TABLE IF EXISTS `tbl_polling`;

CREATE TABLE `tbl_polling` (
  `polling_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `polling_response` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`polling_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_polling` */

/*Table structure for table `tbl_service_order` */

DROP TABLE IF EXISTS `tbl_service_order`;

CREATE TABLE `tbl_service_order` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `contract_doc` varchar(100) DEFAULT NULL,
  `contract_desc` varchar(100) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_service_order` */

/*Table structure for table `tbl_society` */

DROP TABLE IF EXISTS `tbl_society`;

CREATE TABLE `tbl_society` (
  `society_id` int(11) NOT NULL AUTO_INCREMENT,
  `society_name` varchar(20) NOT NULL,
  `society_address` varchar(30) NOT NULL,
  `no_of_building` char(10) NOT NULL,
  PRIMARY KEY (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_society` */

/*Table structure for table `tbl_society_member` */

DROP TABLE IF EXISTS `tbl_society_member`;

CREATE TABLE `tbl_society_member` (
  `sm_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `mobile_no` varchar(15) DEFAULT NULL,
  `no_of_family_mem` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `image` longblob,
  `username` varchar(10) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `confirmatn_sent_at` datetime DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `flat_id` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `log_id` int(11) DEFAULT NULL,
  `membership` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`sm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_society_member` */

insert  into `tbl_society_member`(`sm_id`,`firstname`,`lastname`,`address`,`email`,`mobile_no`,`no_of_family_mem`,`owner_id`,`image`,`username`,`password`,`confirmatn_sent_at`,`confirmed_at`,`flat_id`,`provider_id`,`log_id`,`membership`) values 
(1,'Pooja','Kurup',NULL,'pooja@gmail.com','87897899',NULL,NULL,NULL,'pooja','p123','2016-03-29 00:00:00',NULL,1,NULL,NULL,'Owner'),
(2,'Jyoti','Jagtap',NULL,'jj@gmail.com','68789798',NULL,1,NULL,'jyoti','j123','2016-03-29 00:00:00',NULL,1,NULL,NULL,'Tenant');

/*Table structure for table `tbl_society_rules` */

DROP TABLE IF EXISTS `tbl_society_rules`;

CREATE TABLE `tbl_society_rules` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `description` varchar(50) NOT NULL,
  `cm_id` int(11) NOT NULL,
  `society_id` int(11) NOT NULL,
  PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_society_rules` */

/*Table structure for table `tbl_vehicle_reg` */

DROP TABLE IF EXISTS `tbl_vehicle_reg`;

CREATE TABLE `tbl_vehicle_reg` (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_type` varchar(10) NOT NULL,
  `vehicle_no` varchar(15) DEFAULT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `vechicle_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_vehicle_reg` */

/*Table structure for table `tbl_vendor_details` */

DROP TABLE IF EXISTS `tbl_vendor_details`;

CREATE TABLE `tbl_vendor_details` (
  `vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(20) NOT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `email_id` varchar(30) DEFAULT NULL,
  `pancard_no` varchar(15) DEFAULT NULL,
  `vendor_type` varchar(15) NOT NULL,
  `vendor_address` varchar(50) NOT NULL,
  `service_tax_regno` varchar(10) DEFAULT NULL,
  `TDS` varchar(10) DEFAULT NULL,
  `username` varchar(15) DEFAULT NULL,
  `vendor_password` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`vendor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_vendor_details` */

insert  into `tbl_vendor_details`(`vendor_id`,`vendor_name`,`contact_no`,`email_id`,`pancard_no`,`vendor_type`,`vendor_address`,`service_tax_regno`,`TDS`,`username`,`vendor_password`) values 
(1,' ashish lenghe',' 78945+625',' ashish@gmail.com',' dgh454845',' builder',' bhosari',' 2',' 100',' ashish',' ashish'),
(2,' ved prakash',' 7852145632',' ved@gmail.com',' dhs24263',' builder',' nigdi',' 2%',' 12%',' ved',' ved'),
(3,' hemant patil',' 748512555',' hemant@gmail.com',' ASV452858',' builder',' sangvi',' 3%',' 12%',' hemant',' hemant'),
(4,' sagar',' 74158963',' null',' ASD45412',' builder',' ',' PUNE',' 2',' sagar',' sagar');

/* Procedure structure for procedure `sp_add_achievement` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_achievement` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_achievement`(
in achievement varchar(20),
in description varchar(50))
BEGIN
Insert into tbl_achievment (achievment,description)values(achievement,description);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_bill_payment` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_bill_payment` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_bill_payment`(in bill_date date,
in amount decimal(18,2),
in bill_status varchar(10),
in bill_type varchar(10),
in description varchar(10),
in chequeno varchar(15),
in bill_comment varchar(15))
BEGIN
Insert into tbl_bill_payment (bill_date,amount,bill_status,bill_type,
description,chequeno,bill_comment) values
(bill_date,amount,bill_status,bill_type,
description,chequeno,bill_comment);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_booking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_booking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_booking`(in description varchar(30))
BEGIN
Insert into tbl_booking (description) values
(description);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_classified` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_classified` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_classified`(in productnm varchar(15),
in prod_desc varchar(50),
in prod_image varchar(500),
in pro_cost decimal(18,2),
in valid_upto date
)
BEGIN
Insert into tbl_classified 
(product_name,prod_description,prod_image,prod_cost,valid_upto) 
values
(productnm,prod_desc,prod_image,pro_cost,valid_upto);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_complaint` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_complaint` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_complaint`(
 in `subject` varchar(15),
 in description varchar(100),
 in `read`varchar(10),
 in complaint_status varchar(10),
 in complaint_type varchar(20),
 in complaint_date date)
BEGIN
Insert into tbl_complaint
 (Subject,description,`read`,complaint_status,complaint_type,complaint_date)
 values
 
 (`subject`,description,`read`,complaint_status,complaint_type,complaint_date);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_event` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_event` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_event`(in eventtype varchar(10),
 in eventname varchar(15),
 in eventdate date,
 in eventtime varchar(30)
 )
BEGIN
 Insert into tbl_event (event_type,event_name,event_date,event_time)
 values
 (eventtype,eventname,eventdate,eventtime
 );
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_event_expense` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_event_expense` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_event_expense`(
in exp_desc varchar(50),
in amount decimal(18,2),
in amount_type varchar(20),
in exp_date date
)
BEGIN
Insert into tbl_event_expense (exp_desc,amount,amount_type,exp_date) 
values
(exp_desc,amount,amount_type,exp_date);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_event_income` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_event_income` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_event_income`(in amount decimal(18,2),
in incomedate date,
in amount_type varchar(10))
BEGIN
Insert into tbl_event_income (amount,date,amount_type) values
(amount,incomedate,amount_type);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_forum` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_forum` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_forum`(
in title varchar(50),
in description varchar(100),
in Date date)
BEGIN
Insert into tbl_forum (title,description,date)
values(title,description,Date);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_forumcomment` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_forumcomment` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_forumcomment`(
in comment varchar(100),
in date date)
BEGIN
Insert into tbl_forum_comment (comment,date)values(comment,date);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_log_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_log_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_log_details`(
in newsm_id int(11),
in csignin_ip varchar(20)
)
BEGIN
DECLARE csigin_at datetime;
DECLARE sigin_ip varchar(20);
DECLARE sigin_at datetime;
SET csigin_at = current_date();
IF (newsm_id != 0) THEN
IF EXISTS ( SELECT * FROM tbl_log_details WHERE sm_id=newsm_id) THEN
SELECT current_signin_ip INTO sigin_ip FROM tbl_log_details WHERE sm_id=newsm_id;
SELECT current_sigin_at INTO sigin_at FROM tbl_log_details WHERE sm_id=newsm_id;
UPDATE tbl_log_details SET 
current_signin_ip=csignin_ip,
previous_sigin_ip=sigin_ip,
current_sigin_at=csigin_at,
previous_sigin_at=sigin_at
WHERE sm_id=newsm_id;
ELSE
INSERT INTO tbl_log_details(current_signin_ip,current_sigin_at,sm_id) VALUES(csignin_ip,csigin_at,newsm_id);
END IF;
END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_maintenanceamount` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_maintenanceamount` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_maintenanceamount`(
in effectiveto date,
in effectivefrom date,
in amount int)
BEGIN
Insert into tbl_mnt_amount(effective_to,effective_from,amount)values
(effectiveto,effectivefrom,amount);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_maintenancecharge` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_maintenancecharge` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_maintenancecharge`(
in mntdesc varchar(50),
in mntbaseamount float(18,2),
in mntotherfund float(18,2),
in mntsinkingamt float(18,2),
in todate date,
in fromdate date,
in status varchar(10))
BEGIN
Insert into tbl_maintenance_charge(mnt_desc,mnt_baseamount,mnt_otherfund,mnt_sinkingamt,to_date,from_date,status)
values(mntdesc,mntbaseamount,mntotherfund,mntsinkingamt,todate,fromdate,status);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_maintenance_invoice` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_maintenance_invoice` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_maintenance_invoice`(
    in invoice_date date,
    in payment_mode varchar(15),
    in note varchar(100))
BEGIN
	 INSERT INTO  tbl_maintenence_invoice
	(invoice_date,payment_mode,note) VALUES
	(invoice_date,payment_mode,note);
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_meeting` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_meeting` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_meeting`(
in meetingdate date,
in title varchar(30),
in points_dicussed varchar(100),
in conclusion varchar(80))
BEGIN
Insert into tbl_meeting_tracking (meeting_date,title,points_discussed,conclusion)values(meetingdate,title,points_dicussed,conclusion);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_members_family` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_members_family` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_members_family`(
in firstname varchar(20) ,
in lastname varchar(20),
in age int,
in profession varchar(50),
in rel_with_owner varchar(10),
in gender varchar(10),
in dob date,
in pancard varchar(15))
BEGIN
Insert into tbl_members_family (firstname,lastname,age,profession,
rel_with_owner,gender,dob,pancard_no) 
values(firstname,lastname,age,profession,rel_with_owner,gender,
dob,pancard);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_notice` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_notice` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_notice`(
in validuntile date,
in notice_title varchar(15),
in sub varchar(50),
in description varchar(40),
in attach varchar(200))
BEGIN
DECLARE noticedate DATE;
SET noticedate = CURRENT_DATE();
Insert into tbl_noticeboard(notice_date,valid_until,notice_tile,subject,description,attachment)
values(noticedate,validuntile,notice_title,sub,description,attach);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_panaltycharge` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_panaltycharge` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_panaltycharge`(
in penaltydesc varchar(50),
in penaltyoverduedate date,
in penaltyoverduedays int)
BEGIN
Insert into tbl_penalty_charge(penalty_desc,penalty_over_duedate,penalty_over_due_days)values
(penaltydesc,penaltyoverduedate,penaltyoverduedays);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_polling` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_polling` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_polling`(in polling_response varchar(5) )
BEGIN
Insert into tbl_polling (polling_response) values
(polling_response);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_profile_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_profile_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_profile_details`(
in new_sm_id INT(11),
in new_firstname varchar(20) ,
in new_lastname varchar(20),
in new_address varchar(50),
in new_email varchar(30),
in new_mobile_no varchar(15),
in new_dob date,
in new_age int,
in new_gender varchar(10),
in new_profession varchar(50),
in new_pancard_no varchar(15),
in new_username varchar(10),
in new_password varchar(20))
BEGIN
update tbl_society_member set firstname=new_firstname, lastname=new_lastname, address=new_address,
email=new_email, mobile_no=new_mobile_no, username=new_username, password=new_password where sm_id=new_sm_id;
commit;
update tbl_members_family set firstname=new_firstname, lastname=new_lastname, dob=new_dob, age=new_age, 
gender=new_gender, profession=new_profession, pancard_no=new_pancard_no where sm_id=new_sm_id;

END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_reg_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_reg_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_reg_details`(
in phase_name varchar(10),
in subphase varchar(10),
in flat_no varchar(5),
in floor int(11),
in firstname varchar(20),
in lastname varchar(20),
in email varchar(30),
in mobile_no varchar(15),
in membership varchar(10),
in fowner_id int(11)
)
BEGIN
DECLARE rconfirmatn_sent_at datetime;
DECLARE flat_unique_id INT(11); 
DECLARE unique_sm_id INT(11);
SET rconfirmatn_sent_at = current_date();

IF (phase_name = "empty") THEN
select flat_id into flat_unique_id from tbl_society_member where sm_id=fowner_id;
ELSE
insert into tbl_flat (phase_name,subphase,flat_no,floor) values(phase_name,subphase,flat_no,floor);
commit;
select max(flat_id) into flat_unique_id from tbl_flat;
END IF;
/*select flat_id into flat_id from tbl_flat where phase_name=phase_name and subphase=subphase and flat_no=flat_no and  floor=floor limit 1;*/
insert into tbl_society_member (firstname,lastname,email,mobile_no,membership,owner_id,flat_id,confirmatn_sent_at) values (firstname,lastname,email,mobile_no,membership,fowner_id,flat_unique_id,rconfirmatn_sent_at);
commit;
select max(sm_id) into unique_sm_id from tbl_society_member;
insert into tbl_members_family (sm_id,firstname,lastname) values (unique_sm_id,firstname,lastname);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_service_order` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_service_order` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_service_order`(in start_date date,
in end_date date,
in contract_doc varchar(100),
in contract_desc varchar(100))
BEGIN
Insert into tbl_service_order 
(start_date,end_date,contract_doc,contract_desc) values
(start_date,end_date,contract_doc,contract_desc);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_society_member` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_society_member` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_society_member`(in firstname varchar(20),
in lastname varchar(20),
in address varchar(50),
in email varchar(50),
in mobile_no varchar(15),
in no_of_family_mem int,
in Is_tenant int,
in username varchar(10),
in password varchar(80)
)
BEGIN
Insert into tbl_society_member (firstname,lastname,address,
email,mobile_no,no_of_family_mem,Is_tenant,username,password) values
(firstname,lastname,address,email,mobile_no,no_of_family_mem,Is_tenant,
username,password);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_vehicle_reg` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_vehicle_reg` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_vehicle_reg`(in vehicle_type varchar(10),
 in vehicle_no varchar(15),
 in vechicleimg varchar(500))
BEGIN
Insert into tbl_vehicle_reg (vehicle_type,vehicle_no,vechicle_img) values
(vehicle_type,vehicle_no,vechicleimg);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_vendor` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_vendor` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_vendor`(in vendor_name varchar(20),
in contact_no varchar(15),
in email_id varchar(30),
in pancard_no varchar(15),
in vendor_type varchar(15),
in vendor_address varchar(50),
in service_tax_regno varchar(10),
in TDS varchar(10),
in username varchar(15),
in vpassword varchar(15))
BEGIN
Insert into tbl_vendor_details (vendor_name,contact_no,email_id,
pancard_no,vendor_type,vendor_address,service_tax_regno,TDS,username,vendor_password) 
values
(vendor_name,contact_no,email_id,
pancard_no,vendor_type,vendor_address,service_tax_regno,TDS,username,vpassword);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_achievementdetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_achievementdetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_achievementdetails`()
BEGIN
select achievment,description from tbl_achievment;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_bill_payment` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_bill_payment` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_bill_payment`()
BEGIN
select bill_date,amount,bill_status,bill_type,description,chequeno,bill_comment from 
tbl_bill_payment;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_booking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_booking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_booking`()
BEGIN
	select description from tbl_booking;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_classified` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_classified` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_classified`()
BEGIN
SELECT product_name, prod_description, prod_image,
  prod_cost,valid_upto, s.firstname ,s.lastname , s.mobile_no FROM tbl_classified c
  JOIN tbl_society_member s ON c.sm_id=s.sm_id;
  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_complaintdetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_complaintdetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_complaintdetails`()
BEGIN
select Subject,description,`read`,complaint_status,complaint_type,complaint_date from tbl_complaint;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_event` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_event` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_event`()
BEGIN
select event_type, event_name, event_date, event_time from tbl_event;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_event_expense` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_event_expense` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_event_expense`()
BEGIN
select exp_desc,amount,amount_type,exp_date,exp_date from tbl_event_expense;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_event_income` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_event_income` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_event_income`()
BEGIN
select amount,date,amount_type from tbl_event_income;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_flatdetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_flatdetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_flatdetails`()
BEGIN
select sm.firstname,sm.lastname,sm.membership,f.flat_no
from tbl_society_member sm join tbl_flat f
on f.flat_id=sm.flat_id
where sm.username Is Not Null AND sm.password Is Not Null;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_forumcommentdetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_forumcommentdetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_forumcommentdetails`()
BEGIN
SELECT COMMENT,DATE FROM tbl_forum_comment;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_forumdetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_forumdetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_forumdetails`()
BEGIN
select title,description,date from tbl_forum;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_maintenanceamountdetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_maintenanceamountdetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_maintenanceamountdetails`()
BEGIN
select effective_to,effective_from,amount from tbl_mnt_amount;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_maintenancechargedetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_maintenancechargedetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_maintenancechargedetails`()
BEGIN
SELECT (mnt_baseamount+mnt_otherfund+mnt_sinkingamt) AS "toalamount" ,mnt_desc,to_date,from_date,
STATUS FROM tbl_maintenance_charge;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_maintenance_invoice` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_maintenance_invoice` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_maintenance_invoice`()
BEGIN
	select invoice_date,payment_mode,note from tbl_maintenence_invoice;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_meetingdetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_meetingdetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_meetingdetails`()
BEGIN
select meeting_date ,title, points_discussed ,conclusion from tbl_meeting_tracking;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_members_family` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_members_family` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_members_family`()
BEGIN
select firstname,lastname,age,profession,rel_with_owner,
gender,dob,pancard_no from tbl_members_family ;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_noticeboarddetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_noticeboarddetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_noticeboarddetails`()
BEGIN
select notice_id,notice_date,notice_tile,subject,description,attachment from tbl_noticeboard ;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_penaltychargedetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_penaltychargedetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_penaltychargedetails`()
BEGIN
select penalty_desc,penalty_over_duedate,penalty_over_due_days from tbl_penalty_charge;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_polling` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_polling` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_polling`()
BEGIN
select polling_response from tbl_polling;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_profile_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_profile_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_profile_details`(
in new_sm_id int(11),
in new_firstname varchar(20)
)
BEGIN
SELECT sm.sm_id,sm.firstname,sm.lastname,sm.address,sm.email,sm.mobile_no,mf.dob,mf.age,mf.gender,mf.profession,mf.pancard_no,sm.username,sm.password,ld.is_admin
FROM tbl_society_member sm
JOIN tbl_members_family mf ON mf.sm_id=sm.sm_id AND mf.firstname=sm.firstname
JOIN tbl_log_details ld ON ld.sm_id=sm.sm_id
WHERE sm.sm_id=new_sm_id AND sm.firstname=new_firstname;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_registeredUser` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_registeredUser` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_registeredUser`()
BEGIN
SELECT sm.firstname,sm.lastname,sm.mobile_no,sm.membership,sm.email,f.flat_no,f.phase_name,f.subphase,f.floor,ld.is_admin
FROM tbl_society_member sm 
join tbl_flat f on f.flat_id=sm.flat_id 
join tbl_log_details ld on ld.sm_id=sm.sm_id
where sm.username IS NOT NULL AND sm.password IS NOT NULL;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_service_order` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_service_order` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_service_order`()
BEGIN
select start_date,end_date,contract_doc,contract_desc from tbl_service_order;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_society_member` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_society_member` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_society_member`()
BEGIN
select firstname,lastname,address,email,mobile_no,
no_of_family_mem,Is_tenant,image,username,password
 from tbl_society_member;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_userDetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_userDetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_userDetails`()
BEGIN
SELECT sm.firstname,sm.lastname,sm.mobile_no,sm.membership,sm.email,f.flat_no,f.phase_name,f.subphase,f.floor
FROM tbl_society_member sm join tbl_flat f 
on f.flat_id=sm.flat_id 
where sm.username IS NULL AND sm.password IS NULL;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_vehicle_reg` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_vehicle_reg` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_vehicle_reg`()
BEGIN
select vehicle_type,vehicle_no,vechicle_img from tbl_vehicle_reg;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_vendor` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_get_vendor` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_vendor`()
BEGIN
select vendor_name,contact_no,email_id,pancard_no,vendor_type,
vendor_address,
service_tax_regno,TDS,username,vendor_password from tbl_vendor_details;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
